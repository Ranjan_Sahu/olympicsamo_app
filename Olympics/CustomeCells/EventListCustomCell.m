//
//  EventListCustomCell.m
//  Olympics
//
//  Created by webwerks on 7/26/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "EventListCustomCell.h"

@implementation EventListCustomCell
@synthesize lblEventNameText,lblPositionText,lblFieldText, btnFormSubmitted;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if(IS_IPAD){
        _btnFormSubmittedHeight.constant = 30;
        [btnFormSubmitted.titleLabel setFont: app_font15];
        [lblEventNameText setFont: app_font24];
        [lblPositionText setFont: app_font18];
        [lblFieldText setFont: app_font18];
    }
    else
    {
        _btnFormSubmittedHeight.constant = 22;
        [btnFormSubmitted.titleLabel setFont:app_font_bold_13];
    }
    
    [btnFormSubmitted setUserInteractionEnabled:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
