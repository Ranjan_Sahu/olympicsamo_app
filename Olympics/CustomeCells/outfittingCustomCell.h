//
//  outfittingCustomCell.h
//  Olympics
//
//  Created by Vicky on 18/08/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface outfittingCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *StyleCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *GarmentLabel;
@property (weak, nonatomic) IBOutlet UIView *sizeMainView;
//@property (weak, nonatomic) IBOutlet UILabel *isAlowedValueLabel;
@property (weak, nonatomic) IBOutlet UITextField *isAlowedValueTextField;
@property (weak, nonatomic) IBOutlet UILabel *sizeTextLabel;

@property (weak, nonatomic) IBOutlet UITextField *notesTextField;

@property (weak, nonatomic) IBOutlet UILabel *allowedTextLabel;

@property (weak, nonatomic) IBOutlet UILabel *sizeValueLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeSizeButton;


@end
