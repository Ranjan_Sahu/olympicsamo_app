//
//  addressListTableViewCell.h
//  Olympics
//
//  Created by webwerks2 on 7/12/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@interface addressListTableViewCell : MGSwipeTableCell<MGSwipeTableCellDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblAddressName;
@property (strong, nonatomic) IBOutlet UILabel *lblDisplayAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblLock;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstraint;



@end
