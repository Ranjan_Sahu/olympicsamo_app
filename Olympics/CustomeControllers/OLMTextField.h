//
//  UITextFieldView.h
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OLMTextField;
@protocol OLMTextFieldDelegate<NSObject>
@required
-(void)OLMTextFieldDidBeginEditing:(UITextField*)textField;
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField;
@end


@interface OLMTextField : UIView<UITextFieldDelegate>
{
        NSMutableDictionary *contollerInfo;
}
@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property (nonatomic) NSInteger questionTypeId;
@property(nonatomic,retain) UITextField *textFieldView;
@property(nonatomic,retain) UILabel *questionLbl;
@property (weak)id<OLMTextFieldDelegate>delegate;
@property (assign, nonatomic) BOOL isQuestionNeeded;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired defaultValue:(NSString*)defaultValue keyboardType:(UIKeyboardType)keyboard delegate:(id)sender;
-(NSDictionary*)GetAnswerInfo;
@end
