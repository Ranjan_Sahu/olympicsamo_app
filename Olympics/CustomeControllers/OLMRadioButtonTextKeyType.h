//
//  UIRadioButton.h
//  MIRadioButtonGroup
//
//  Created by Vikas Karambalkar on 25/10/14.
//
//

#import <UIKit/UIKit.h>
@class OLMRadioButtonTextKeyType;
@protocol OLMRadioButtonTextKeyTypeDelegate<NSObject>
@required
-(void)OLMRadioButtonTextKeyType:(OLMRadioButtonTextKeyType*)radioButton didSelectOptionAtIndex:(NSInteger)index;
@end


@interface OLMRadioButtonTextKeyType : UIView
{
    NSMutableArray *radioButtons;
    NSMutableDictionary *contollerInfo;
    NSArray *optionsArray;
}
@property (weak)id<OLMRadioButtonTextKeyTypeDelegate>delegate;
@property (nonatomic) NSInteger increamentedHeight;
@property (nonatomic) NSInteger currentSelectedIndex;
@property (nonatomic) NSInteger questionTypeId;
@property(nonatomic) NSInteger IsRequired;


-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId options:(NSArray*)optionsList IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)defaultValue delegate:(id)sender;

-(NSDictionary*)GetAnswerInfo;

@end
