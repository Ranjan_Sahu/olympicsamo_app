//
//  Constant.h
//
//  Created by Vikas Karambalkar on 05/07/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#ifndef Olympics_Constant_h
#define Olympics_Constant_h
#import "CommonSettings.h"
#import "AppDelegate.h"

#define COMMON_SETTINGS [CommonSettings sharedInstance]
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

//Current Device screen information
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


//Service URL's
#define weburl @"https://zeusapi.plus1percent.com/api/"

//ServiceNames
#define Service_AccountDetails @"AccountDetails/Authenticate"
#define Service_ForgotPassword @"AccountDetails/ForgotPassword"
#define Service_SocialLoginApi @"AccountDetails/SocialLoginApi"

// Personal Details Service
#define Service_PersonalDetails @"Personal/GetPersonalAccrDetails"
#define Service_PersonalHeaderDetails @"Personal/GetPersonalHeaderDetails"
#define Service_InsertUpdatePersonalAccrDetails @"Personal/InsertUpdatePersonalAccrDetails"
#define Service_PersonalAccreditationImageUpload @"Personal/AccreditationImageUpload"

// Email Service
#define Service_GetEmailDetails @"Email/GetEmailAccrDetails"
#define Service_DeleteEmailDetails @"Email/DeleteEmailAccrDetails"
#define Service_EditEmailDetails @"Email/EditEmailAccrDetails"
#define Service_InsertUpdateEmailDetails @"Email/InsertUpdateEmailAccrDetails"

// Address Service
#define Service_AddressDetails @"Address/GetAddressAccrDetails"
#define Service_DeleteAddressDetails @"Address/DeleteAddressAccrDetails"
#define Service_EditAddressDetails @"Address/EditAddressAccrDetails"
#define Service_InsertUpdateAddressDetails @"Address/InsertUpdateAddressAccrDetails"

// Notification Service
#define Service_GetNotificationList @"Notification/GetNotificationList"
#define Service_GetViewNotificationDetails @"Notification/GetViewNotification"
#define Service_GetNotificationCountDetails @"Notification/GetNotificationCount"
#define Service_InsertUpdateMsgViewDetails @"Notification/InsertUpdateMsgView"
#define Service_DeleteNotification @"Notification/DeleteNotification"
#define Service_UpdateNotificationViewCount @"Notification/UpdateNotificationViewCount"

// Contact Service
#define Service_GetContactDetails @"Phone/GetPhoneAccrDetails"
#define Service_DeleteContactDetails @"Phone/DeletePhoneAccrDetails"
#define Service_EditContactDetails @"Phone/EditPhoneAccrDetails"
#define Service_InsertUpdateContactDetails  @"Phone/InsertUpdatePhoneAccrDetails"

// Games Service
#define Service_GamesDetails @"Games/GetGamesAccrDetails"
#define Service_InsertUpadteGamesAccrDetails @"Games/InsertUpadteGamesAccrDetails"

// Residence Service
#define Service_ResidenceAccrDetails @"Residence/GetResidenceAccrDetails"
#define Service_InsertUpdateResidenceDetails @"Residence/InsertUpdateResidenceDetails"

// Financial Service
#define Service_FinancialAccrDetails @"Financials/GetFinancialAccrDetails"
#define Service_InsertUpdateFinancialDetails @"Financials/InsertUpdateFinancialDetails"

// Passport Service
#define Service_PassportAccrDetails @"Passport/GetPassportAccrDetails"
#define Service_InsertUpdatePassportDetails @"Passport/InsertUpdatePassportDetails"

// Events Service
#define Service_GetEventAccrDetails @"Event/GetEventAccrDetails"
#define Service_GetEventDropdownDetails @"Event/GetEventDropdownDetails"
#define Service_EditEventDetails @"Event/GetEventAccrEditDetails"
#define Service_InsertUpdateEventDetails @"Event/InsertUpdateEventAccrDetails"
#define Service_DeleteEventDetails @"Event/DeleteEventAccrDetails"

// NextOfKin Service
#define Service_GetNextOfKinAccrDetails @"NextOfKin/GetNextOfKinAccrDetails"
#define Service_EditNextOfKinList @"NextOfKin/EditNextOfKinAccrDetails"
#define Service_InsertUpdateNextOfKinAccrDetails @"NextOfKin/InsertUpdateNextOfKinAccrDetails"
#define Service_DeleteNextOfKinAccrDetails @"NextOfKin/DeleteNextOfKinAccrDetails"

// Sports Service
#define Service_SportDisciplineDetail @"SportDiscipline/GetSportDisciplineAccrDetails"
#define Service_InsertUpdateSportDiscipline @"SportDiscipline/InsertUpdateSportDisciplineDetails"
#define Service_DeleteSportDiscipline @"SportDiscipline/DeleteSportDiscipline"

// Team Service
#define Service_GetTeamAccrDetails @"Team/GetTeamAccrDetails"
#define Service_InsertUpdateTeamDetails @"Team/InsertUpdateTeamDetails"
#define Service_DeleteTeamDetails @"Team/DeleteTeamDetails"

//measurement Service
#define Service_MeasurementAccrDetails @"Measurement/GetMeasurementAccrDetails"
#define Service_InsertUpdateMeasurementAccrDetails @"Measurement/InsertUpdateMeasurementAccrDetails"

// My Team Service
#define Service_GetMyTeamAccrDetails @"MyTeam/GetMyTeamAccrDetails"

// Disability Service
#define Service_GetDisabilityAccrDetails @"Disability/GetDisabilityAccrDetails"
#define Service_InsertUpdateDisabilityDetails @"Disability/InsertUpdateDisabilityDetails"

// OutFitting Service
#define Service_GetOutfittingAccrDetails @"Outfitting/GetOutfittingAccrDetails"
#define Service_InsertUpdateOutfitting @"Outfitting/InsertUpdateOutfittingAccrDetails"

// Adhoc Service
#define Service_AdhocQuestionAccrDetails @"AdhocQuestion/GetAdhocQuestionAccrDetails"
#define Service_InsertUpdateAdhocQuestionDetails @"AdhocQuestion/InsertUpdateAdhocQuestionDetails"

//InformationServices
#define Service_GetInformationList @"Information/GetInformationList"

// Service Social SignIn
#define Service_SocialSignInAccountDetails @"AccountDetails/Authenticate"

// Service Agreement
#define Service_GetAgreementDetails @"Agreement/GetAgreementAccrDetails"
#define Service_InsertUpdateTeamAgreementDetails @"Agreement/InsertUpdateAgreementsDetails"

// Service MyGames
#define Service_GetMyGamesDetails @"Games/GetGamesList"

// Service MainMenuVisibility
#define Service_GetMainMenuVisibilityList @"AccountDetails/GetMainMenuVisibility"

// Service Logout
#define Service_Logout @"AccountDetails/Logout"


//ServiceAuthentication Parameters
//#define authValue  @"anRpc3lzdGVtczEyMyFAIw=="
//#define MACAddress @"MACAddress"

//NSUserDefautls Keys
#define UD_UserInfo @"UserInfoDictionary"
#define UD_Userid @"UserName"
#define UD_Password @"Password"
#define UD_ThumbnailProfileImage @"ThumbnailProfileImage"
#define UD_FullSizeProfileImage @"FullSizeProfileImage"
#define UD_UserSecretPin @"UserSecretPin"
#define UD_GameImage @"GameURL"
#define UD_IsAgreement @"IsAgreement"
#define UD_IsInformation @"IsInformation"
#define UD_IsMyTeamVisible @"IsMyTeamVisible"
#define UD_IsPersonal @"IsPersonal"
#define UD_ArrayMyTeamListString @"ArrayMyTeamListString"
#define UD_MyTeamListCurrentIndex @"MyTeamListCurrentIndex"
#define UD_SelectedPlayerInfo @"SelectedPlayerInfo"
#define Game_ImageURL @"Game_ImageURL"
#define Device_Token @"Device_Token"
#define Game_Id @"Game_ID"
#define Resource_Id @"Resource_ID"

//AlertMessageTitle
#define Alert_Title @"Olympics Says.!"
#define Alert_Notification_Title @"Notification.!"


//Local Alert Messages
#define Alert_NoInterNet @"Please check Internet Access "
#define Alert_NoResponse @"No response received, Please try after some time "
#define Alert_Error @"Some Error Occured In Response ! Please try after some time."


//Application Font For Dynamic Lables
// Regular Fonts
#define app_font10  [UIFont fontWithName:@"Lato-Regular" size:10]
#define app_font12  [UIFont fontWithName:@"Lato-Regular" size:12]
#define app_font13  [UIFont fontWithName:@"Lato-Regular" size:13]
#define app_font15  [UIFont fontWithName:@"Lato-Regular" size:15]
#define app_font16  [UIFont fontWithName:@"Lato-Regular" size:16]
#define app_font18  [UIFont fontWithName:@"Lato-Regular" size:18]
#define app_font19  [UIFont fontWithName:@"Lato-Regular" size:19]
#define app_font20  [UIFont fontWithName:@"Lato-Regular" size:20]
#define app_font21  [UIFont fontWithName:@"Lato-Regular" size:21]
#define app_font23  [UIFont fontWithName:@"Lato-Regular" size:23]
#define app_font24  [UIFont fontWithName:@"Lato-Regular" size:24]
#define app_font32  [UIFont fontWithName:@"Lato-Regular" size:32]
#define app_font27  [UIFont fontWithName:@"Lato-Regular" size:27]

// Bold Fonts
#define app_font_bold_10  [UIFont fontWithName:@"Lato-Bold" size:10]
#define app_font_bold_12  [UIFont fontWithName:@"Lato-Bold" size:12]
#define app_font_bold_13  [UIFont fontWithName:@"Lato-Bold" size:13]
#define app_font_bold_15  [UIFont fontWithName:@"Lato-Bold" size:15]
#define app_font_bold_16  [UIFont fontWithName:@"Lato-Bold" size:16]
#define app_font_bold_18  [UIFont fontWithName:@"Lato-Bold" size:18]
#define app_font_bold_20  [UIFont fontWithName:@"Lato-Bold" size:20]
#define app_font_bold_21  [UIFont fontWithName:@"Lato-Bold" size:21]
#define app_font_bold_22  [UIFont fontWithName:@"Lato-Bold" size:22]
#define app_font_bold_24  [UIFont fontWithName:@"Lato-Bold" size:24]
#define app_font_bold_27  [UIFont fontWithName:@"Lato-Bold" size:27]


// Application Colors
#define color_black [UIColor blackColor]
#define color_gray  [UIColor colorWithRed:0.2588 green:0.2588 blue:0.2588 alpha:1.0] 
#define color_lightGray [UIColor colorWithRed:0.7000 green:0.7000 blue:0.7000 alpha:1.0]
#define color_white  [UIColor whiteColor]
#define color_red [UIColor redColor]
#define color_blue [UIColor blueColor]
#define color_blueFont [UIColor colorWithRed:0.0000 green:0.5176 blue:0.8392 alpha:1.0]
#define color_orange  [UIColor colorWithRed:0.8941 green:0.3569 blue:0.2118 alpha:1.0]

// Form Controls
#define Field_TextField 0
#define Field_DropdownList 1
#define Field_RadioButton 2
#define Field_DatePicker 3
#define Field_CheckBox 4
#define Field_NumericTextField 5
#define Field_TextLabel 6
#define Field_TextKeyValueList 7
#define Field_SingleCheckBox 8
#define Field_ImagePickerView 9

//Adhoc Form Controls
#define AHFormatText 1  //(Normal Text Box)
#define AHFormatNumeric 2 //(Text Box with numeric value only)
#define AHFormatDate 3 //(Date Picker )
#define AHFormatYesNo 4 //(Radio button)
#define AHFormatOption 5 //(Drop down list)
#define AHFormatMulti 7 //
#define AHFormatTextMultiline 8 //(Multiline text box)
#define AHFormatSingleCheckBox 9 //(Radio button)


// Collection View Fields
#define FieldSectionId_PERSONAL 1
#define FieldSectionId_GAMES 2
#define FieldSectionId_DISABILITY 3
#define FieldSectionId_EMAIL 4
#define FieldSectionId_ADDRESS 5
#define FieldSectionId_PHONE 6
#define FieldSectionId_RESIDENCE 7
#define FieldSectionId_PASSPORT 8
#define FieldSectionId_NEXT_OF_KIN 9
#define FieldSectionId_FINANCIAL 10
#define FieldSectionId_SPORT 49
#define FieldSectionId_ACCREDITATION 50
#define FieldSectionId_EVENTS 51
#define FieldSectionId_TEAM 52
#define FieldSectionId_ADHOC 180
#define FieldSectionId_MEASUREMENT 181
#define FieldSectionId_OUTFITTING 182

#define Type_ID_Facebook 0
#define Type_ID_Twitter 1
#define Type_ID_Gmail 2

//Get_details or Delete_Detals From Server.
//typedef enum {
//    
//    Get_details,
//    
//    Delete_details,
//
//    Edit_details
//    
//}Service_type;//enum

//Get_details or Delete_Detals From Server.
typedef enum {
    
    Service_Called_Add,
    Service_Called_Save,
    Service_Called_GetDetail,
    Service_Called_Edit,
    Service_Called_Delete,
    Service_Called_Login,
    Service_Called_ForgotPassword,
    Service_Called_SocialLogin,
    Service_Called_NotificationCount,
    Service_Called_Update,
    Service_Called_Get_Game_Detail,
    Service_Called_Logout
}Service_Called;//enum

#endif
