

#import "Service.h"
#import "Base64.h"
#import "Constant.h"

@implementation Service
@synthesize delegate;

#pragma mark -
#pragma mark Web response methods

-(void)requestingURLString:(NSString*)URLString Service:(NSString*)ServiceName withParameters:(NSMutableDictionary *)param ContactID:(NSString*)ContactID
{
    if ([ServiceName isEqualToString:Service_Logout]) {
        NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
        if(userinfo)
        {
            [param setValue:[userinfo valueForKey:@"UserID"] forKey:@"UserID"];
        }
        [param setValue:@2 forKey:@"OSID"];
        [param setValue:@"AMO" forKey:@"TenantId"];
    }
    else{
    
        if ([ServiceName isEqualToString:Service_AccountDetails] || [ServiceName isEqualToString:Service_SocialLoginApi]) {
            [param setValue:@"" forKey:@"GamesId"];
            [param setValue:@2 forKey:@"OSID"];
            [param setValue:[[NSUserDefaults standardUserDefaults] valueForKey:Device_Token] forKey:@"DeviceToken"];
        }
        else if ([ServiceName isEqualToString:Service_GetMyGamesDetails]){
            [param setValue:@"" forKey:@"GamesId"];
        }
        else{
            [param setValue:[[NSUserDefaults standardUserDefaults] objectForKey:Game_Id] forKey:@"GamesId"];
        }
    
        //Fixed Parameter
        [param setValue:@1 forKey:@"LId"];
        [param setValue:@1 forKey:@"SiteId"];
        [param setValue:@"AMO" forKey:@"TenantId"];
        ContactID = [NSString stringWithFormat:@"%@",ContactID];
        if(ContactID && [ContactID length]>0)
            [param setValue:[NSNumber numberWithInt:[ContactID intValue]] forKey:@"ContactID"];

        NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
        if(userinfo){
            [param setValue:[userinfo valueForKey:@"UserID"] forKey:@"UserID"];
        }
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData)
    {
        //NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }

    //URL formation
    NSString *FinalURL = [URLString stringByAppendingString:ServiceName];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    
    //Headers
    NSDictionary *headers;
    headers = @{ @"content-type": @"application/json",
                 @"cache-control": @"no-cache",
                 @"postman-token": @"a57fd802-9c61-2aeb-6d9c-f714d1842b88" };
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:FinalURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSString *authValue = [self GenerateKeyForBasicAuthentication];
    if([authValue length] > 0)
         [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error)
                                          {
                                              if(error.code == -1009)
                                              {
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                      [delegate no_Response_Function];

                                                  });
                                              }
                                              else
                                              {
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                      [delegate Service_Error:error];
                                                      
                                                  });
                                              }
                                          }
                                          else
                                          {
                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                                  NSString *serverResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                  [delegate Service_Success:serverResponse];
                                              });
                                          }
                                      }];
    [dataTask resume];
}

-(void)requestingNonUserInfoURLString:(NSString*)URLString Service:(NSString*)ServiceName withParameters:(NSMutableDictionary *)param
{
    NSString *gameId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Game_ID"];
    [param setValue:gameId forKey:@"GamesId"];

    //Fixed Parameter
    [param setValue:@1 forKey:@"LId"];
    [param setValue:@1 forKey:@"SiteId"];
    //[param setValue:@"3016" forKey:@"GamesId"];
    [param setValue:@"AMO" forKey:@"TenantId"];
    
    //URL formation
    NSString *FinalURL = [URLString stringByAppendingString:ServiceName];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    
    //Headers
    NSDictionary *headers;
    headers = @{ @"content-type": @"application/json",
                 @"cache-control": @"no-cache",
                 @"postman-token": @"a57fd802-9c61-2aeb-6d9c-f714d1842b88" };
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:FinalURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error)
                                          {
                                              if(error.code == -1009)
                                              {
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                      [delegate no_Response_Function];
                                                      
                                                  });
                                              }
                                              else
                                              {
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                      [delegate Service_Error:error];
                                                      
                                                  });
                                              }
                                          }
                                          else
                                          {
                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                                  NSString *serverResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                  [delegate Service_Success:serverResponse];
                                              });
                                          }
                                      }];
    [dataTask resume];
}

-(void)requestingURLString:(NSString*)URLString Service:(NSString*)ServiceName withParameters:(NSMutableDictionary *)param ContactID:(NSString*)ContactID imagesDict:(NSDictionary *)imgDict{
    
        
        //Fixed Parameter
        [param setValue:@"1" forKey:@"LId"];
        [param setValue:@"1" forKey:@"SiteId"];
        [param setValue:@"AMO" forKey:@"TenantId"];
        [param setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:Game_Id]] forKey:@"GamesId"];

        ContactID = [NSString stringWithFormat:@"%@",ContactID];
        if(ContactID && [ContactID length]>0)
            [param setValue:ContactID forKey:@"ContactID"];
        else{
            [param setValue:@"0" forKey:@"ContactID"];
        }
        NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
        if(userinfo){
            [param setValue:[NSString stringWithFormat:@"%@",[userinfo valueForKey:@"UserID"]] forKey:@"UserID"];
        }
  
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    NSString *FinalURL = [URLString stringByAppendingString:ServiceName];
    NSURL *serverUrl = [NSURL URLWithString: FinalURL];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSString *endBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", BoundaryConstant];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    for(NSString *key in param){
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[param[key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[endBoundary dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    
    for(NSString *key in imgDict){
        NSData *imageData = UIImageJPEGRepresentation(imgDict[key], 1.0);
        if (imageData) {
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"1_capture.jpg\"\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
     [request setURL:serverUrl];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if (error)
        {
            if(error.code == -1009)
            {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [delegate no_Response_Function];
                    
                });
            }
            else
            {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [delegate Service_Error:error];
                    
                });
            }
        }
        else
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                NSString *serverResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                [delegate Service_Success:serverResponse];
            });
        }
    }];
    [postDataTask resume];
}

-(NSString*)GenerateKeyForBasicAuthentication
{
    NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
    if(userinfo)
    {
        NSString *ContactID = [userinfo valueForKey:@"ContactID"];
        NSString *TenantId = [userinfo valueForKey:@"TenantId"];
        NSString *BasicAuthUid = [userinfo valueForKey:@"BasicAuthUid"];
        NSString*hashStr = [NSString stringWithFormat:@"%@_%@:%@",ContactID,TenantId,BasicAuthUid];
        NSString *secondtstr = [hashStr base64EncodedString];
        secondtstr  = [NSString stringWithFormat:@"Basic %@",secondtstr];
        return secondtstr;
    }
    return @"";
}
@end
