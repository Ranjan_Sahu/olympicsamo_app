//
//  AddEventViewController.h
//  Olympics
//
//  Created by webwerks2 on 7/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMSingleCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "Service.h"

@interface AddEventViewController : UIViewController<Service_delegate>

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (nonatomic) NSInteger incrementedHeight;
@property (nonatomic, strong) NSMutableArray *controlls;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (nonatomic, strong) NSArray *eventTypeArray;
@property (nonatomic,assign) Service_Called serviceCalled;
@property (nonatomic, strong) NSDictionary *eventDetails;
@property (nonatomic, strong) UIViewController *owner;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end
