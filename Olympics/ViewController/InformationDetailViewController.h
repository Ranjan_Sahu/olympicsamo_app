//
//  InformationDetailViewController
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InformationListCustomCell.h"
@interface InformationDetailViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *vanigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogi;

@property (strong, nonatomic) NSMutableArray *controlls;
@property (strong, nonatomic) NSMutableDictionary *infromationDetailDictionary;
@property (strong, nonatomic) NSMutableArray *fieldsToShowArray;
@property (strong, nonatomic) NSDictionary *playerDetails;
@property (assign, nonatomic) BOOL isLogedInUsersDetail;
@property (nonatomic) NSInteger incrementedHeight;
@property (nonatomic) UIView *viewInfoDetailsHeader;

- (IBAction)DidSelectBackButton:(id)sender;


@end
