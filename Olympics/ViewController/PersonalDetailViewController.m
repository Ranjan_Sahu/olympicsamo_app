//
//  PersonalDetailViewController.m
//  Olympics
//
//  Created by webwerks on 07/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "PersonalDetailViewController.h"
#import "UIImageView+WebCache.h"

@interface PersonalDetailViewController ()
{
    //    int constetnForViewCollection;
    //    int constentForExpandedView;
    int MaxScroll;
}
@end

@implementation PersonalDetailViewController

@synthesize previousLocation;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setProfileImages];
    _navigatiobBarLogo.text = @"\ue90c";
    _userEmailAddress.text = @"";
    _userMobileNo.text = @"";
    _userName.text = @"";
    _userSport.text = @"";
    
    MaxScroll = 240;
    if(IS_IPAD)
        MaxScroll = 400;

    
    [self.subViewCollectionView registerNib:[UINib nibWithNibName:@"myProfileCollectionViewCell" bundle:nil] forCellWithReuseIdentifier: @"myProfileCollectionViewCell"];
    //[self CallServiceTogetSubMenues];

    UIPanGestureRecognizer *panGesture1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.expendView setUserInteractionEnabled:YES];
    [self.expendView addGestureRecognizer:panGesture1];
    
    UIPanGestureRecognizer *panGesture2 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.viewHeader setUserInteractionEnabled:YES];
    [self.viewHeader addGestureRecognizer:panGesture2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpSubviews];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:TRUE];
    [self setProfileImages];
    
    if (IS_IPHONE_5) {
        _heightCollectionView.constant = 360;
    }
    else if (IS_IPHONE_6) {
        _heightCollectionView.constant = 430;
    }
    else if (IS_IPHONE_6P) {
        _heightCollectionView.constant = 500;
    }
    if (IS_IPAD) {
        _heightCollectionView.constant = 650;
    }
    [self CallServiceTogetSubMenues];
}

-(void)setProfileImages{
    
    if(_isLogedInUsersDetail)
    {
        [ self SetImagesFromURLThumbImage:[[NSUserDefaults standardUserDefaults] valueForKey:UD_ThumbnailProfileImage] FullSizeImage:[[NSUserDefaults standardUserDefaults] valueForKey:UD_FullSizeProfileImage]];
    }
    else
    {
        [ self SetImagesFromURLThumbImage:[_playerDetails valueForKey:@"ThumbnailProfileImage"] FullSizeImage:[_playerDetails valueForKey:@"FullSizeProfileImage"]];
    }

}
-(void)setUpSubviews
{
    [self.imgUserProfileSmall.layer setCornerRadius:self.imgUserProfileSmall.frame.size.width/2];
    [self.imgUserProfileSmall.layer setMasksToBounds:YES];
}

-(void)viewDidLayoutSubviews
{
    if(previousGesture)
    {
        [self handlePanGesture:previousGesture];
    }
    else
    {
        CGRect newFrame = self.expendView.frame;
        newFrame.size.height = MaxScroll;
        self.expendView.frame=newFrame;
        
        CGRect viewForDetailFrame = self.viewHeader.frame;
        viewForDetailFrame = CGRectMake(viewForDetailFrame.origin.x, newFrame.size.height -  viewForDetailFrame.size.height, viewForDetailFrame.size.width, viewForDetailFrame.size.height);
        self.viewHeader.frame = viewForDetailFrame;
        self.viewCollection.frame = CGRectMake(self.viewCollection.frame.origin.x, newFrame.origin.y+newFrame.size.height, self.viewCollection.frame.size.width, self.viewCollection.frame.size.height);
        
        _constraintFullImage.constant = newFrame.size.height;
        [self.view updateConstraintsIfNeeded];
        [self.view layoutIfNeeded];
    }
}

#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    NSMutableDictionary *parameters  = [[NSMutableDictionary alloc] init];
    if(_isLogedInUsersDetail)
    {
        [parameters setValue:@false forKey:@"IsMyTeamMember"];
    }
    else
    {
        [parameters setValue:@true forKey:@"IsMyTeamMember"];
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_PersonalHeaderDetails withParameters:parameters ContactID:[_playerDetails valueForKey:@"ContactID"]];
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
    {
        [self callMethodToSetHeaderView:responseDictionery];
        
        NSMutableArray *responseMenuArray = [responseDictionery objectForKey:@"lstPersonalHeader"];
        subMenuArray = [[NSMutableArray alloc]init];
        for(id key in responseMenuArray)
        {
            if([[key valueForKey:@"Visibility"] boolValue] == TRUE)
            {
                [subMenuArray addObject:key];
            }
        }
        if([subMenuArray count] > 0)
        {
            [self.subViewCollectionView reloadData];
        }
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma - mark collectionView Deleget

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [subMenuArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myProfileCollectionViewCell";
    
    myProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.titleLebel.text = [[subMenuArray objectAtIndex:indexPath.item] valueForKey:@"lblText"];
    [cell.submenuLogoLabel setText:[self getImageForfieldId:[[[subMenuArray objectAtIndex:indexPath.item] valueForKey:@"FieldSectionId"] integerValue]]];
    
    if([[[subMenuArray objectAtIndex:indexPath.item] valueForKey:@"IsFilled"] intValue] == 1)
    {
        cell.visibilityMark.hidden = FALSE;
        cell.alpha = 1.0;
    }
    else
    {
        cell.visibilityMark.hidden = TRUE;
        cell.alpha = 0.5;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
        return CGSizeMake(150, 150);
    else if (IS_IPHONE_5)
        return CGSizeMake(89, 89);
    else
        return CGSizeMake(108, 108);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger FieldSectionId = [[[subMenuArray objectAtIndex:indexPath.item] valueForKey:@"FieldSectionId"] integerValue];
    NSInteger Visibility = [[[subMenuArray objectAtIndex:indexPath.item] valueForKey:@"Visibility"] integerValue];
    
    if(Visibility)
    {
        switch (FieldSectionId) {
                
            case FieldSectionId_PERSONAL:
            {
                DynamicFormViewController *hsVC=[[DynamicFormViewController alloc]initWithNibName:@"DynamicFormViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_GAMES:
            {
                GamesViewController *hsVC=[[GamesViewController alloc]initWithNibName:@"GamesViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_DISABILITY:
            {
                DisabilityViewController *hsVC=[[DisabilityViewController alloc]initWithNibName:@"DisabilityViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_EMAIL:
            {
                EmailListingViewController *hsVC=[[EmailListingViewController alloc]initWithNibName:@"EmailListingViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_ADDRESS:
            {
                AddressListingViewController *hsVC=[[AddressListingViewController alloc]initWithNibName:@"AddressListingViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_PHONE:
            {
                ContactListngViewController *hsVC=[[ContactListngViewController alloc]initWithNibName:@"ContactListngViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_RESIDENCE:
            {
                ResidenceViewController *hsVC=[[ResidenceViewController alloc]initWithNibName:@"ResidenceViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_PASSPORT:
            {
                PassportViewController *hsVC=[[PassportViewController alloc]initWithNibName:@"PassportViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_NEXT_OF_KIN:
            {
                NextOfKinListViewController *hsVC=[[NextOfKinListViewController alloc]initWithNibName:@"NextOfKinListViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_FINANCIAL:
            {
                FinancialViewController *hsVC=[[FinancialViewController alloc]initWithNibName:@"FinancialViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_SPORT:
            {
                SportsListingViewController *hsVC=[[SportsListingViewController alloc]initWithNibName:@"SportsListingViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_ACCREDITATION:
            {
                AccreditationViewController *hsVC=[[AccreditationViewController alloc]initWithNibName:@"AccreditationViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_EVENTS:
            {
                EventListViewController *hsVC=[[EventListViewController alloc]initWithNibName:@"EventListViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_TEAM:
            {
                TeamListingViewController *hsVC=[[TeamListingViewController alloc]initWithNibName:@"TeamListingViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_ADHOC:
            {
                AdhocViewController *hsVC=[[AdhocViewController alloc]initWithNibName:@"AdhocViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_MEASUREMENT:
            {
                MeasurementViewController *hsVC=[[MeasurementViewController alloc]initWithNibName:@"MeasurementViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            case FieldSectionId_OUTFITTING:
            {
                OutfitingViewController *hsVC=[[OutfitingViewController alloc]initWithNibName:@"OutfitingViewController" bundle:nil];
                hsVC.isLogedInUsersDetail = _isLogedInUsersDetail;
                hsVC.playerDetails = _playerDetails;
                [self.navigationController pushViewController:hsVC animated:YES];
            }
                break;
                
            default:
            {
            }
                break;
        }
    }
}

#pragma mark - Set Logo images Method

-(NSString*)getImageForfieldId:(NSInteger)fieldId
{
    
    NSString *title;
    switch (fieldId) {
        case FieldSectionId_PERSONAL:
            title = @"\ue915";
            break;
        case FieldSectionId_GAMES:
            title = @"\ue909";
            break;
        case FieldSectionId_DISABILITY:
            title = @"\ue905";
            break;
        case FieldSectionId_EMAIL:
            title = @"\ue918";
            break;
        case FieldSectionId_ADDRESS:
            title = @"\ue910";
            break;
        case FieldSectionId_PHONE:
            title = @"\ue901";
            break;
        case FieldSectionId_RESIDENCE:
            title = @"\ue90b";
            break;
        case FieldSectionId_PASSPORT:
            title = @"\ue914";
            break;
        case FieldSectionId_NEXT_OF_KIN:
            title = @"\ue911";
            break;
        case FieldSectionId_FINANCIAL:
            title = @"\ue908";
            break;
        case FieldSectionId_SPORT:
            title = @"\ue91d";
            break;
        case FieldSectionId_ACCREDITATION:
            title = @"\ue90f";
            break;
        case FieldSectionId_EVENTS:
            title = @"\ue906";
            break;
        case FieldSectionId_TEAM:
            title = @"\ue912";
            break;
        case FieldSectionId_ADHOC:
            title = @"\ue924";
            break;
        case FieldSectionId_MEASUREMENT:
            title = @"\ue91b";
            break;
        case FieldSectionId_OUTFITTING:
            title = @"\ue928";
            break;
        default:
            title = @"NF";
            break;
    }
    
    return title;
}

-(void)SetImagesFromURLThumbImage:(NSString*)thumbImage FullSizeImage:(NSString*)fullsizeImage
{
    if(![thumbImage isKindOfClass:[NSNull class]] && ![thumbImage isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_imgUserProfileSmall sd_setImageWithURL:[NSURL URLWithString:thumbImage] placeholderImage:[UIImage imageNamed:@"testImage"]];
            [_imgUserLargePic sd_setImageWithURL:[NSURL URLWithString:thumbImage] placeholderImage:[UIImage imageNamed:@"testImage"]];
        });
    }
    
    if(![fullsizeImage isKindOfClass:[NSNull class]] && ![fullsizeImage isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_imgUserLargePic sd_setImageWithURL:[NSURL URLWithString:fullsizeImage] placeholderImage:[UIImage imageNamed:@"testImage"]];
        });
    }
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) {
//        
//        if(![thumbImage isKindOfClass:[NSNull class]])
//        {
//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbImage]];
//            UIImage* image = [[UIImage alloc] initWithData:imageData];
//            if (image) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    _imgUserProfileSmall.image = image;
//                    _imgUserLargePic.image = image;
//                });
//            }
//        }
//        
//        if(![fullsizeImage isKindOfClass:[NSNull class]])
//        {
//            NSData *imageDataBig = [NSData dataWithContentsOfURL:[NSURL URLWithString:fullsizeImage]];
//            UIImage* imageBig = [[UIImage alloc] initWithData:imageDataBig];
//            if (imageBig) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    _imgUserLargePic.image = imageBig;
//                });
//            }
//        }
//    });
}

#pragma mark - Pangesture Method

-(void)handlePanGesture:(UIPanGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateBegan)
    {
        previousLocation = [sender locationInView:self.view];
    }
    else if ([sender state] == UIGestureRecognizerStateChanged)
    {
        previousGesture = sender;
        
        CGPoint temp = [sender locationInView:self.view];
        if(temp.y > previousLocation.y)
        {
            CGRect newFrame = self.expendView.frame;
            newFrame.size.height=newFrame.size.height + ((temp.y - previousLocation.y)*1.0);
            
            if(newFrame.size.height < [UIScreen mainScreen].bounds.size.height )
            {
                [self.view layoutIfNeeded];
                self.expendView.frame=newFrame;
                CGRect viewForDetailFrame = self.viewHeader.frame;
                viewForDetailFrame = CGRectMake(viewForDetailFrame.origin.x, newFrame.size.height -  viewForDetailFrame.size.height, viewForDetailFrame.size.width, viewForDetailFrame.size.height);
                self.viewHeader.frame = viewForDetailFrame;
                self.viewCollection.frame = CGRectMake(self.viewCollection.frame.origin.x, newFrame.origin.y+newFrame.size.height, self.viewCollection.frame.size.width, self.viewCollection.frame.size.height);
                _constraintFullImage.constant = newFrame.size.height;
            }
            previousLocation = [sender locationInView:self.view];
        }
        else if(temp.y < previousLocation.y)
        {
            CGRect newFrame = self.expendView.frame;
            newFrame.size.height=newFrame.size.height - ((previousLocation.y - temp.y)*1.0);
            
            if(newFrame.size.height > MaxScroll)
            {
                [self.view layoutIfNeeded];
                self.expendView.frame=newFrame;
                CGRect viewForDetailFrame = self.viewHeader.frame;
                viewForDetailFrame = CGRectMake(viewForDetailFrame.origin.x, newFrame.size.height -  viewForDetailFrame.size.height, viewForDetailFrame.size.width, viewForDetailFrame.size.height);
                self.viewHeader.frame = viewForDetailFrame;
                self.viewCollection.frame = CGRectMake(self.viewCollection.frame.origin.x, newFrame.origin.y+newFrame.size.height, self.viewCollection.frame.size.width, self.viewCollection.frame.size.height);
                _constraintFullImage.constant = newFrame.size.height;
                
            }
            previousLocation = [sender locationInView:self.view];
        }
        savedNewFrame = self.expendView.frame;
    }
    else if ( [sender state] == UIGestureRecognizerStatePossible)
    {
        CGRect newFrame = savedNewFrame;
        newFrame.size.height=newFrame.size.height ;
        
        if(newFrame.size.height < [UIScreen mainScreen].bounds.size.height )
        {
            [self.view layoutIfNeeded];
            self.expendView.frame=newFrame;
            CGRect viewForDetailFrame = self.viewHeader.frame;
            viewForDetailFrame = CGRectMake(viewForDetailFrame.origin.x, newFrame.size.height -  viewForDetailFrame.size.height, viewForDetailFrame.size.width, viewForDetailFrame.size.height);
            self.viewHeader.frame = viewForDetailFrame;
            self.viewCollection.frame = CGRectMake(self.viewCollection.frame.origin.x, newFrame.origin.y+newFrame.size.height, self.viewCollection.frame.size.width, self.viewCollection.frame.size.height);
            _constraintFullImage.constant = newFrame.size.height;
            
        }
        previousLocation = [sender locationInView:self.view];
    }
}

#pragma mark - IBAction Methods

- (IBAction)DidSelectBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Method To set HeaderView

-(void) callMethodToSetHeaderView:(NSMutableDictionary *)responseDict
{
    _userEmailAddress.text = [[responseDict valueForKey:@"Email"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Email"];
    _userMobileNo.text = [[responseDict valueForKey:@"Phone"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Phone"];
    _userName.text = [[responseDict valueForKey:@"FullName"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"FullName"];
    _userSport.text = [[responseDict valueForKey:@"Sport"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Sport"];
    
    if(_isLogedInUsersDetail)
    {
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo]];
        
        [userInfo setValue:[[responseDict valueForKey:@"Sport"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Sport"] forKey:@"Discipline"];
        [userInfo setValue:[[responseDict valueForKey:@"Phone"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Phone"] forKey:@"Phone"];
        [userInfo setValue:[[responseDict valueForKey:@"FullName"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"FullName"] forKey:@"FullName"];
        [userInfo setValue:[[responseDict valueForKey:@"Email"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Email"] forKey:@"Email"];
        
        [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:UD_UserInfo];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        NSData *data = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_ArrayMyTeamListString] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableArray *arrayTeamList = [[NSMutableArray alloc]initWithArray:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
        NSMutableDictionary *selectedPlayerData = [[NSMutableDictionary alloc]initWithDictionary:[arrayTeamList objectAtIndex:[[[NSUserDefaults standardUserDefaults] valueForKey:UD_MyTeamListCurrentIndex] integerValue]]];
        
        [selectedPlayerData setValue:[[responseDict valueForKey:@"Sport"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Sport"] forKey:@"Discipline"];
        [selectedPlayerData setValue:[[responseDict valueForKey:@"Phone"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Phone"] forKey:@"Phone"];
        [selectedPlayerData setValue:[[responseDict valueForKey:@"FullName"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"FullName"] forKey:@"FullName"];
        [selectedPlayerData setValue:[[responseDict valueForKey:@"Email"] isKindOfClass:[NSNull class]]?@"":[responseDict valueForKey:@"Email"] forKey:@"Email"];
        
        [arrayTeamList replaceObjectAtIndex:[[[NSUserDefaults standardUserDefaults] valueForKey:UD_MyTeamListCurrentIndex] integerValue] withObject:selectedPlayerData];
        NSError *error;
        NSData *arrayMyTeamListjsonData = [NSJSONSerialization dataWithJSONObject:arrayTeamList options:NSJSONWritingPrettyPrinted error: &error];
        NSString *arrayMyTeamListString = [[NSString alloc] initWithData:arrayMyTeamListjsonData encoding:NSUTF8StringEncoding];
        [[NSUserDefaults standardUserDefaults] setObject:arrayMyTeamListString forKey:UD_ArrayMyTeamListString];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

@end
