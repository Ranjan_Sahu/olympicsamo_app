//
//  AddEventViewController.m
//  Olympics
//
//  Created by webwerks2 on 7/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AddEventViewController.h"
#import "EventListViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
@interface AddEventViewController ()
{
    CGFloat animatedDistance;
    BOOL getEventDropDownList;
    NSMutableArray *eventsDropDownArray;
    NSString *previousText,*currentText;
}

@end

@implementation AddEventViewController
@synthesize incrementedHeight, controlls ,eventTypeArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _titleLabel.font = app_font_bold_15;
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue906"];
    self.isChangesMade = FALSE;
    
    //Create Require Fields.
    [self CallServiceToGetEventDropDownList];
}

-(void) CallServiceToGetEventDropDownList {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"TeamID" : @"0"
                                 };
    
    getEventDropDownList = YES;
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetEventDropdownDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if ([self.owner class]==[EventListViewController class]) {
        [(EventListViewController *)self.owner setIsNeedToRefresh:FALSE];
    }
    
    if(self.isChangesMade)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

#pragma - mark WebService Call
-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    
    if(_serviceCalled == Service_Called_Edit){
        
        [userinformation setValue:[NSString stringWithFormat:@"%@",[_eventDetails valueForKey:@"EventEntryID"]] forKey:@"EventEntryID"];
        [userinformation setValue:[NSString stringWithFormat:@"%@",[_eventDetails valueForKey:@"TeamId"]] forKey:@"TeamId"];
    }
    else if (_serviceCalled == Service_Called_Add){
        
        [userinformation setValue:@"0" forKey:@"EventEntryID"];
        [userinformation setValue:@"0" forKey:@"TeamId"];
        
    }
    else{
        
        //NSLog(@"Not Valid type found!!");
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateEventDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(getEventDropDownList)
    {
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            getEventDropDownList = FALSE;
            eventsDropDownArray = [responseDictionery objectForKey:@"lstEventDropdown"];
            if(eventsDropDownArray){
                [self StartAddingControllers];
            }
            else{
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"EventDropDown array not found !" CancelButtonTitle:@"OK" InView:self];
            }
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else
    {
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if ([self.owner class]==[EventListViewController class]) {
                [(EventListViewController *)self.owner setIsNeedToRefresh:TRUE];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers {
    
    //Field_DropdownList.
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    
    if(![eventsDropDownArray isKindOfClass:[NSNull class]] && eventsDropDownArray && [eventsDropDownArray count]>0)
    {
        NSInteger initialIndex = 0;
        if(_serviceCalled == Service_Called_Edit){
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"Value == %@",_eventDetails[@"EventId"]];
            
            NSArray *filteredValues = [eventsDropDownArray filteredArrayUsingPredicate:filterPredicate];
            
            if(filteredValues.count){
                initialIndex = [[filteredValues lastObject][@"Key"]integerValue];
            }
        }
        
        OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Event" fieldIdName:@"EventId" questionId:101 Options:eventsDropDownArray IsRequired:YES defaultValue:initialIndex delegate:self];
        controlObj.questionTypeId = Field_DropdownList;
        controlObj.tag = 0;
        [controlls addObject:controlObj];
        [self.baseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    // Chech_Box_Field.
    OLMSingleCheckBox *checkBoxOneObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:102 fieldIdName:@"EntryFormSubmitted" OptionTitle:@"Entry Form Submitted" isRequired:NO defaultSelected:[_eventDetails[@"EntryFormSubmitted"]boolValue] delegate:self];
    checkBoxOneObj.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj.tag = 11;
    [controlls addObject:checkBoxOneObj];
    [self.baseScrollView addSubview:checkBoxOneObj];
    incrementedHeight = incrementedHeight + checkBoxOneObj.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Position" fieldIdName:@"PositionInField" questionId:103 IsRequired:NO defaultValue:(_serviceCalled == Service_Called_Edit) ?(_eventDetails[@"PositionInField"]?_eventDetails[@"PositionInField"]:@""):@"" keyboardType:UIKeyboardTypeNumberPad  delegate:self];
    controlObj.questionTypeId = Field_NumericTextField;
    controlObj.tag = 1;
    controlObj.textFieldView.text = (_serviceCalled == Service_Called_Edit) ?_eventDetails[@"PositionInField"]:@"";
    [controlls addObject:controlObj];
    [self.baseScrollView addSubview:controlObj];
    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj1 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Field" fieldIdName:@"SizeOfField" questionId:104 IsRequired:NO defaultValue:(_serviceCalled == Service_Called_Edit) ?(_eventDetails[@"SizeOfField"]?_eventDetails[@"SizeOfField"]:@""):@"" keyboardType:UIKeyboardTypeNumberPad delegate:self];
    controlObj1.questionTypeId = Field_NumericTextField;
    controlObj1.tag = 2;
    [controlls addObject:controlObj1];
    [self.baseScrollView addSubview:controlObj1];
    incrementedHeight = incrementedHeight + controlObj1.frame.size.height + 5;
    
    // Chech_Box_Field.
    OLMSingleCheckBox *checkBoxOneObj1 = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:105 fieldIdName:@"Medal" OptionTitle:@"Medals" isRequired:NO defaultSelected:[_eventDetails[@"Medal"]boolValue] delegate:self];
    checkBoxOneObj1.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj1.tag = 12;
    [controlls addObject:checkBoxOneObj1];
    [self.baseScrollView addSubview:checkBoxOneObj1];
    incrementedHeight = incrementedHeight + checkBoxOneObj1.frame.size.height + 5;
    
    // Chech_Box_Field.
    OLMSingleCheckBox *checkBoxOneObj2 = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:106 fieldIdName:@"Diploma" OptionTitle:@"Diploma" isRequired:NO defaultSelected:[_eventDetails[@"Diploma"]boolValue] delegate:self];
    checkBoxOneObj2.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj2.tag = 13;
    [controlls addObject:checkBoxOneObj2];
    [self.baseScrollView addSubview:checkBoxOneObj2];
    incrementedHeight = incrementedHeight + checkBoxOneObj2.frame.size.height + 5;
    
    // Chech_Box_Field.
    OLMSingleCheckBox *checkBoxOneObj3 = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:107 fieldIdName:@"Official" OptionTitle:@"Official" isRequired:NO defaultSelected:[_eventDetails[@"Official"]boolValue] delegate:self];
    checkBoxOneObj3.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj3.tag = 14;
    [controlls addObject:checkBoxOneObj3];
    [self.baseScrollView addSubview:checkBoxOneObj3];
    incrementedHeight = incrementedHeight + checkBoxOneObj3.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj2 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Note" fieldIdName:@"Notes" questionId:108 IsRequired:NO defaultValue:(_serviceCalled == Service_Called_Edit) ?(_eventDetails[@"Notes"]?_eventDetails[@"Notes"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self];
    controlObj2.questionTypeId = Field_TextField;
    controlObj2.tag = 3;
    controlObj2.textFieldView.text = (_serviceCalled == Service_Called_Edit) ?_eventDetails[@"Notes"]:@"";
    [controlls addObject:controlObj2];
    [self.baseScrollView addSubview:controlObj2];
    incrementedHeight = incrementedHeight + controlObj2.frame.size.height + 5;
    
    self.isChangesMade = FALSE;
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMSingleCheckBox:(OLMSingleCheckBox *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}

#pragma mark - OLMTextView Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
