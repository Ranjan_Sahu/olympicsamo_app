//
//  AccreditationViewController.m
//  Olympics
//
//  Created by webwerks on 14/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AccreditationViewController.h"
#import "PECropViewController.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "Constant.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface AccreditationViewController ()
{
    UIImageView *largeImageView;
    UIView *zoomView;
    NSDictionary *answersDict;
    OLMImagePickerView *IMGcontrolObj;
}
@end

@implementation AccreditationViewController
@synthesize incrementedHeight, controlls;


- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirstTime = true;
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue90f"];
    // Do any additional setup after loading the view from its nib.
    
   }

-(void)viewDidAppear:(BOOL)animated
{
    if(_isFirstTime)
    {
        [self StartAddingControllers];
        //Set User Image from image Url
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo];
        NSString *thumbImage = [[NSUserDefaults standardUserDefaults] valueForKey:UD_ThumbnailProfileImage];
        
        if(![thumbImage isKindOfClass:[NSNull class]] && ![thumbImage isEqualToString:@""])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [IMGcontrolObj.imageView sd_setImageWithURL:[NSURL URLWithString:thumbImage] placeholderImage:[UIImage imageNamed:@"photo_box"]];
            });
        }

        _isFirstTime = false;
    }
//    //Set User Image from image Url
//    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo];
//    NSString *thumbImage = [userInfo valueForKey:UD_ThumbnailProfileImage];
//    
//    if(![thumbImage isKindOfClass:[NSNull class]] && ![thumbImage isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [IMGcontrolObj.imageView sd_setImageWithURL:[NSURL URLWithString:thumbImage] placeholderImage:[UIImage imageNamed:@"photo_box"]];
//        });
//    }
}

- (IBAction)DidSelectBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData{
    [COMMON_SETTINGS showHUD];
    
    NSDictionary *imageDict = [[NSDictionary alloc] initWithObjectsAndKeys:IMGcontrolObj.imageView.image, @"filename", nil];
    
    NSString *resourceId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:Resource_Id]];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:resourceId forKey:@"ResourceID"];
    [parameters setValue:@"1_capture.jpg" forKey:@"FileName"];
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_PersonalAccreditationImageUpload withParameters:parameters ContactID:[_playerDetails valueForKey:@"ContactID"] imagesDict:imageDict];
    _serviceCalled = Service_Called_Save;
    
}

-(void)viewWillLayoutSubviews
{
    _BaseScrillViewWidth.constant = self.view.frame.size.width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    
    if (_serviceCalled == Service_Called_Save)
    {
        // [COMMON_SETTINGS hideHUD];
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo];
            NSString *thumbImage = [userInfo valueForKey:UD_ThumbnailProfileImage];
            
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"ResourceID"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"ResourceID"] forKey:Resource_Id];
            
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"ThumbnailProfileImage"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"ThumbnailProfileImage"] forKey:UD_ThumbnailProfileImage];
            
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"FullSizeProfileImage"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"FullSizeProfileImage"] forKey:UD_FullSizeProfileImage];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
          //  [[SDImageCache sharedImageCache] removeImageForKey:thumbImage fromDisk:YES withCompletion:nil];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else{
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Method to add questions on screen

-(void)StartAddingControllers
{
    //Field_DropdownList
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.BaseScrollView.frame;
    
    //Field_ImagePickerView
    IMGcontrolObj = [[OLMImagePickerView alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Select Image" fieldIdName:@"image" questionId:111 IsRequired:YES delegate:self];
    IMGcontrolObj.questionTypeId = Field_ImagePickerView;
    IMGcontrolObj.tag = 10;
    [controlls addObject:IMGcontrolObj];
    [self.BaseScrollView addSubview:IMGcontrolObj];
    incrementedHeight = incrementedHeight + IMGcontrolObj.frame.size.height + 5;
    
    self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
    
}


#pragma mark - OLMImagePicker Delegate
-(void)OLMImagePickerDidDismissWithImage:(UIImage*) image forImageView:(UIImageView*)imageView
{
//    uploadImage = [[UIImage alloc] init];
//    uploadImage = imageView.image;
    [self openEditor:image];
    
}

- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = (id)self;
    controller.image = image1;
    
    UIImage *image = image1;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length * 0.78,
                                          length);
    //controller.cropAspectRatio = 6.0F / 10.0F;
    controller.keepingCropAspectRatio = YES;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

-(void)DidSelectCloseZoomView:(UIButton*)sender
{
    for (UIView *view in zoomView.subviews) {
        [view removeFromSuperview];
    }
    [zoomView removeFromSuperview];
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    NSData *pngData = UIImagePNGRepresentation(croppedImage);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"image.png"]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    
    IMGcontrolObj.imageView.image = croppedImage;
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
       // [self updateEditButtonEnabled];
    }
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.BaseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.BaseScrollView.window convertRect:self.BaseScrollView.bounds fromView:self.BaseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}


@end
