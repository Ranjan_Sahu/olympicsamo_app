//
//  InformationListViewController
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "addressListTableViewCell.h"
#import "OLMTextField.h"
#import "OLMDropDown.h"

// custom controllers Imports
#import "Question_Object.h"
#import "Service.h"

@interface InformationListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MGSwipeTableCellDelegate, Service_delegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (weak, nonatomic) IBOutlet UITableView *tableviewInformationList;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewLeadingSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet UIView *viewSlide;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slideViewWidth;
@property (weak, nonatomic) IBOutlet UIImageView *searchImageView;

@property (nonatomic, assign) Service_Called serviceCalled;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (nonatomic, assign) BOOL isLogedInUsersDetail;

- (IBAction)idiSelectSearchButton:(id)sender;
- (IBAction)DidSelectBackButton:(id)sender;

@end
