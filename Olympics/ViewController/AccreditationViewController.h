//
//  AccreditationViewController.h
//  Olympics
//
//  Created by Manisha Roy on 15/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMImagePickerView.h"
#import "Service.h"


@interface AccreditationViewController : UIViewController<Service_delegate,OLMImagePickerViewDelegate, UIScrollViewDelegate>


@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (weak, nonatomic) IBOutlet UIScrollView *BaseScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BaseScrillViewWidth;
@property (assign, nonatomic) BOOL isFirstTime;
@property(nonatomic, assign) Service_Called serviceCalled;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;


- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end
