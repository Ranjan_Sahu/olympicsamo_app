//
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "NotificationListingViewController.h"
#import "MyGamesViewController.h"
#import "HomeScreenViewController.h"

#define USE_MG_DELEGATE 1
@interface NotificationListingViewController () {
    
    NSMutableArray *swipeCellButtons;
    NSMutableArray *arrNotificationList;
    NSDictionary *notificationDetails;
    UITableViewCellAccessoryType accessory;
    BOOL allowMultipleSwipe;
    NSIndexPath *currentIndexPathToDelete, *currentIndexPathToUpdate;
}

@end

@implementation NotificationListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    swipeCellButtons = [TestData data];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue927"];
    _isFirstTime = true;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.isNeedToRefresh=FALSE;
    self.tableviewAddreslist.backgroundColor = color_white;
    
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if(self.presentingViewController)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
         [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.isNeedToRefresh || _isFirstTime)
    {
        self.isNeedToRefresh=false;
        _isFirstTime = false;
        [self callServiceToGetNotificationslist];
    }
}

#pragma - mark WebService Call
-(void)callServiceToGetNotificationslist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];;
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetNotificationList withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)callServiceDeleteNotification {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];;
    parameters  = @{
                    @"MsgID" : [notificationDetails valueForKey:@"MsgID"]
                    };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteNotification withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_Delete ;
}

-(void)callServiceToUpdateMessageViewCount {
    
    //[COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];;
    parameters  = @{
                    @"MsgID" : [notificationDetails valueForKey:@"MsgID"]
                    };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_UpdateNotificationViewCount withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_Update ;
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    //[COMMON_SETTINGS hideHUD];
    if (_serviceCalled == Service_Called_Delete) {
        [COMMON_SETTINGS hideHUD];
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            if ([self.owner class]==[MyGamesViewController class]){
                [(MyGamesViewController *)self.owner setIsNeedToRefresh:TRUE];
            }
            else if ([self.owner class]==[HomeScreenViewController class])
            {
                [(HomeScreenViewController *)self.owner setIsNeedToRefresh:TRUE];
            }
            //After Getting Success_Response We need delete the Record from Tableview.
            [arrNotificationList removeObjectAtIndex:currentIndexPathToDelete.row];
            [_tableviewAddreslist beginUpdates];
            [_tableviewAddreslist deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [_tableviewAddreslist endUpdates];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if (_serviceCalled == Service_Called_GetDetail) {
        [COMMON_SETTINGS hideHUD];
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstGridNotification"] isKindOfClass:[NSNull class]])
            {
                arrNotificationList = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstGridNotification"]];
                
                if([arrNotificationList count] > 0) {
                    [_tableviewAddreslist reloadData];
                }
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_Update){
        
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            //Show message description
            NSString *messageString;
            messageString = [[NSString alloc] initWithFormat:@"***\nDescription : %@.\n***",[notificationDetails valueForKey:@"MsgDescription"]];
            
            [COMMON_SETTINGS AlertViewWithTitle:[notificationDetails valueForKey:@"MsgTitle"] Message:messageString CancelButtonTitle:@"OK" InView:self];
            
            NotificationCustomCell *selectedCell = (NotificationCustomCell *)[self.tableviewAddreslist cellForRowAtIndexPath:currentIndexPathToUpdate];

            if(IS_IPAD)
            {
                [selectedCell.titleLabel setFont: app_font24];
                [selectedCell.descriptionLabel setFont: app_font20];
            }
            else
            {
                [selectedCell.titleLabel setFont: app_font16];
                [selectedCell.descriptionLabel setFont: app_font13];
            }

            // Set Refresh flag
            if ([self.owner class]==[MyGamesViewController class]){
                [(MyGamesViewController *)self.owner setIsNeedToRefresh:TRUE];
            }
            else if ([self.owner class]==[HomeScreenViewController class])
            {
                [(HomeScreenViewController *)self.owner setIsNeedToRefresh:TRUE];
            }
        }
        else{
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/************************************ TABLEVIEW Functions ****************************************************/
#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrNotificationList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *itemCell = @"NotificationCustomCell";
    NotificationCustomCell *cell = (NotificationCustomCell *)[self.tableviewAddreslist dequeueReusableCellWithIdentifier:itemCell];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCustomCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    CGFloat cellHeight = 20.0f;
    CGFloat constCellHeight = 65.0f;
    if(IS_IPAD)
        constCellHeight = 85.0f;
    
    NSDictionary *dictionary = [arrNotificationList objectAtIndex:indexPath.row];
    
    //NSString *descriptionString = [[dictionary valueForKey:@"PublishedDate"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *titleString = [[dictionary valueForKey:@"MsgTitle"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.titleLabel.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
    
    //CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.descriptionLabel.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
    
    cellHeight = titleHeight + cellHeight;
    
    if(cellHeight > constCellHeight)
        return cellHeight;
    else
        return constCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *itemCell = @"NotificationCustomCell";
    NotificationCustomCell *cell = (NotificationCustomCell *)[self.tableviewAddreslist dequeueReusableCellWithIdentifier:itemCell];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCustomCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dictionary = [arrNotificationList objectAtIndex:indexPath.row];
    
    NSString *descriptionString = [[dictionary valueForKey:@"PublishedAgo"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //NSString *publishedDateString = [[dictionary valueForKey:@"PublishedDate"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    
    NSString *titleString = [[dictionary valueForKey:@"MsgTitle"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.titleLabel.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
    
    //CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.descriptionLabel.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
    
    cell.titleHeightConstraint.constant = titleHeight;
    //cell.descriptionHeightConstraint.constant = descriptionHeight;
    
    cell.titleLabel.text = titleString;
    cell.descriptionLabel.text = descriptionString;
    //cell.descriptionLabel.text = [self calculateDuration:publishedDateString];
    
    if([[dictionary valueForKey:@"IsRead"] integerValue] == 0)
    {
        if(IS_IPAD)
        {
            [cell.titleLabel setFont: app_font_bold_24];
            [cell.descriptionLabel setFont: app_font_bold_20];
        }
        else
        {
            [cell.titleLabel setFont: app_font_bold_16];
            [cell.descriptionLabel setFont: app_font_bold_13];
        }
        
    }
    
    cell.delegate = self;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    currentIndexPathToUpdate = indexPath;
    
    notificationDetails = [arrNotificationList objectAtIndex:indexPath.row];
    [self callServiceToUpdateMessageViewCount];
    
    //NSDictionary *notificationDetail = [arrNotificationList objectAtIndex:indexPath.row];
    //    messageString = [[NSString alloc] initWithFormat:@"***\nDescription : %@.\n***\nPublished On : %@",[notificationDetail valueForKey:@"MsgDescription"],[notificationDetail valueForKey:@"MsgTitle"]];
    
//    NSString *messageString;
//    messageString = [[NSString alloc] initWithFormat:@"***\nDescription : %@.\n***",[notificationDetails valueForKey:@"MsgDescription"]];
//    
//    [COMMON_SETTINGS AlertViewWithTitle:[notificationDetails valueForKey:@"MsgTitle"] Message:messageString CancelButtonTitle:@"OK" InView:self];
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray *result = [NSMutableArray array];
    NSString *titles[1] = {@"DELETE"};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *icons[1] = {delete};
    
    UIColor *colors[2] = {[UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1], [UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1]};
    
    for (int i = 0; i < 1; ++i)
    {
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:data.rightButtonsCount];
    }
}
#endif

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSIndexPath *indexPath = [self.tableviewAddreslist indexPathForCell:cell];
    notificationDetails = [arrNotificationList objectAtIndex:indexPath.row];
    
    if (index == 0)
    {
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete this notification?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDeleteNotification];
                                                                         
                                                                     }];
            
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            // code to be written for version lower than ios 8.0...
        }
    }
    else {
        
    }
    return YES;
}

#pragma mark - Format date to show Relative Time Duration
-(NSString *)calculateDuration:(NSString *)originalDate
{
    // Formate published date
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    [df setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *convertedDate = [df dateFromString:originalDate];
    
    // Formate current date
    NSString *nowDateString = [self getCurrentUTC];
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'Z'"];
    [df1 setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *nowDate = [df dateFromString:nowDateString];
    
    double ti = [convertedDate timeIntervalSinceDate:nowDate];
    ti = ti * -1;
    if(ti < 1) {
        return @"never";
    } else  if (ti < 60) {
        return @"Just Now";
    } else if (ti < 3600) {
        int diff = round(ti / 60);
        return [NSString stringWithFormat:@"%d Minutes ago", diff];
    } else if (ti < 86400) {
        int diff = floor(ti / 60 / 60);
        return[NSString stringWithFormat:@"%d Hours ago", diff];
    } else {
        int diff = floor(ti / 60 / 60 / 24);
        return[NSString stringWithFormat:@"%d Days ago", diff];
    }
    
    //    } else if (ti < 2629743) {
    //        int diff = round(ti / 60 / 60 / 24);
    //        return[NSString stringWithFormat:@"%d Days ago", diff];
    //    } else {
    //        return @"never";
    //    }
}

- (NSString *)getCurrentUTC
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    NSString *localDateString = [dateFormatter stringFromDate:date];
    return localDateString;
}

@end
