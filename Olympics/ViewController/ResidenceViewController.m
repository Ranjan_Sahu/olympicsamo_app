//
//  DisabilityViewController.m
//  Olympics
//
//  Created by webwerks on 14/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "ResidenceViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface ResidenceViewController ()
{
    NSString *previousText,*currentText;
}
@end

@implementation ResidenceViewController
@synthesize incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue90b"];
    self.isChangesMade = FALSE;
    
    [self CallServiceTogetSubMenues];
}

-(void)viewDidAppear:(BOOL)animated
{
}
- (IBAction)DidSelectBackButton:(id)sender {
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillLayoutSubviews
{
    _BaseScrillViewWidth.constant = self.view.frame.size.width;
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers
{
    //Field_DropdownList
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.BaseScrollView.frame;
    
    //Place Text Field
    OLMTextField *PlaceControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Place" fieldIdName:@"Place" questionId:101 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"Place"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"Place"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    PlaceControlObj.questionTypeId = Field_TextField;
    PlaceControlObj.tag = 0;
    [controlls addObject:PlaceControlObj];
    [self.BaseScrollView addSubview:PlaceControlObj];
    incrementedHeight = incrementedHeight + PlaceControlObj.frame.size.height + 5;
    
    //City Text Field
    OLMTextField *CityControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"City" fieldIdName:@"City" questionId:102 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"City"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"City"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    CityControlObj.questionTypeId = Field_TextField;
    CityControlObj.tag = 1;
    [controlls addObject:CityControlObj];
    [self.BaseScrollView addSubview:CityControlObj];
    incrementedHeight = incrementedHeight + CityControlObj.frame.size.height + 5;
    
    //State Dropdown
    if(![_stateList isKindOfClass:[NSNull class]] && _stateList && [_stateList count]>0)
    {
        OLMDropDown *StateControlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"State" fieldIdName:@"StateId" questionId:103 Options:_stateList IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"StateId"] intValue] delegate:self];
        StateControlObj.questionTypeId = Field_DropdownList;
        StateControlObj.tag = 2;
        [controlls addObject:StateControlObj];
        [self.BaseScrollView addSubview:StateControlObj];
        incrementedHeight = incrementedHeight + StateControlObj.frame.size.height + 5;
    }
    else //StateId
    {
        //NSLog(@"Option Array Not Found");
    }
    
    //Zip Text Field
    OLMTextField *ZipControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Zip" fieldIdName:@"PostCode" questionId:104 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"PostCode"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"PostCode"] keyboardType:UIKeyboardTypeNumberPad  delegate:self];
    
    ZipControlObj.questionTypeId = Field_NumericTextField;
    ZipControlObj.tag = 3;
    [controlls addObject:ZipControlObj];
    [self.BaseScrollView addSubview:ZipControlObj];
    incrementedHeight = incrementedHeight + ZipControlObj.frame.size.height + 5;
    
    //Country dropdown
    if(![_countryList isKindOfClass:[NSNull class]] && _countryList && [_countryList count]>0)
    {
        OLMDropDown *CountryControlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Country" fieldIdName:@"CountryId" questionId:105 Options:_countryList IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"CountryId"] intValue] delegate:self];
        CountryControlObj.questionTypeId = Field_DropdownList;
        CountryControlObj.tag = 4;
        [controlls addObject:CountryControlObj];
        [self.BaseScrollView addSubview:CountryControlObj];
        incrementedHeight = incrementedHeight + CountryControlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
    
    self.isChangesMade = FALSE;
}

#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters ;//= @{ @"IsMyTeamMember": @"", @"OrgID": @"" };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_ResidenceAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateResidenceDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Save;
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            _DefaultValues = responseDictionery;
            _countryList = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstddlCountry"]];
            _stateList = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstddlState"]];
            
            if([_countryList count] > 0 && [_stateList count] >0)
            {
                [self StartAddingControllers];
                self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
                [self.view bringSubviewToFront:self.navigationBarView];
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.BaseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.BaseScrollView.window convertRect:self.BaseScrollView.bounds fromView:self.BaseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)scrollDown {
    
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
