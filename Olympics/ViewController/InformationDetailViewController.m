//
//  InformationDetailViewController
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "InformationDetailViewController.h"
#import "InformationListCustomCell.h"

@interface InformationDetailViewController ()
{
    NSInteger webViewContentHeight;
    UIWebView *descriptionPage;
    UIWebView *test;
}
@end

@implementation InformationDetailViewController
@synthesize incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    _navigatiobBarLogi.text = [NSString stringWithFormat:@"\ue90d"];
    self.baseScrollView.backgroundColor = color_white;
    
    webViewContentHeight = 100;
    
    NSString *descriptionHtmlString = [[_infromationDetailDictionary valueForKey:@"TopicDescription"] isKindOfClass:[NSNull class]]?@"":[_infromationDetailDictionary valueForKey:@"TopicDescription"];
    
    if(![descriptionHtmlString isKindOfClass:[NSNull class]])
    {
        test = [[UIWebView alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, webViewContentHeight)];
        test.backgroundColor = [UIColor clearColor];
        test.scrollView.scrollEnabled = NO;
        test.delegate = self;
        [test loadHTMLString:descriptionHtmlString baseURL:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _titleLabel.text = [[_infromationDetailDictionary valueForKey:@"TopicTitle"] isKindOfClass:[NSNull class]]?@"":[_infromationDetailDictionary valueForKey:@"TopicTitle"];
    
    [COMMON_SETTINGS showHUD];
    [self.navigationController.navigationBar setHidden:TRUE];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self StartAddingControllers];
    [COMMON_SETTINGS hideHUD];
}

-(CAShapeLayer*)GetLableForFormSubmitedBackgroundInFrame:(CGRect)frame
{
    NSInteger arrowSize;
    
    if(IS_IPAD){
        arrowSize = 17;
    }else{
        arrowSize = 13;
    }
    //    CGPoint point1 = CGPointMake(0,frame.size.height/2);
    //    CGPoint point2 = CGPointMake(frame.size.height/2, 0);
    //    CGPoint point3 = CGPointMake(frame.size.width, 0);
    //    CGPoint point4 = CGPointMake(frame.size.width, frame.size.height);
    //    CGPoint point5 = CGPointMake(frame.size.height/2, frame.size.height);
    
    CGPoint point1 = CGPointMake(0,0);
    CGPoint point2 = CGPointMake(frame.size.width, 0);
    CGPoint point3 = CGPointMake((frame.size.width+frame.size.height/2), frame.size.height/2);
    CGPoint point4 = CGPointMake(frame.size.width, frame.size.height);
    CGPoint point5 = CGPointMake(0, frame.size.height);
    
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:point1];
    [linePath addLineToPoint:point2];
    [linePath addLineToPoint:point3];
    [linePath addLineToPoint:point4];
    [linePath addLineToPoint:point5];
    [linePath addLineToPoint:point1];
    
    line.lineWidth = 1.5f;
    line.path=linePath.CGPath;
    line.fillColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:0.50].CGColor;
    return line;
}

- (IBAction)DidSelectBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers {
    incrementedHeight = 0;
    
//    NSArray *nib;
//    
//    if(IS_IPAD)
//    {
//        nib = [[NSBundle mainBundle] loadNibNamed:@"InformationListCustomCellIPad" owner:nil options:nil];
//    }
//    else
//    {
//        nib = [[NSBundle mainBundle] loadNibNamed:@"InformationListCustomCell" owner:nil options:nil];
//    }
//    
//    InformationListCustomCell *cell =  [nib objectAtIndex:0];
//    CGRect cellFrame = cell.contentView.frame;
//    cellFrame.size.width = _baseScrollView.frame.size.width;
//    cell.contentView.frame = cellFrame;
//    cell.contentView.backgroundColor = [UIColor colorWithRed:0.8606 green:0.8606 blue:0.8606 alpha:0.8];
//    
//    cell.lblEventNameText.text = [NSString stringWithFormat:@"%@",[_infromationDetailDictionary valueForKey:@"TopicTitle"]];
//    cell.lblFieldText.numberOfLines = 100;
//    cell.lblFieldText.text = [NSString stringWithFormat:@"%@",[[_infromationDetailDictionary valueForKey:@"TopicBlurb"] isKindOfClass:[NSNull class]]?@" ":[_infromationDetailDictionary valueForKey:@"TopicBlurb"]];
//    //cell.DateLabel.text = [COMMON_SETTINGS getDateOnlyString:[_infromationDetailDictionary valueForKey:@"CreatedDate"]];
//    cell.DateLabel.text = [_infromationDetailDictionary valueForKey:@"CreatedDate"];
//
//    [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
//    [cell.btnFormSubmitted setTitle:[_infromationDetailDictionary valueForKey:@"CategoryTitle"] forState:UIControlStateNormal];
//    
//    NSString *formSubmittedString = [cell.btnFormSubmitted.currentTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    
//    CGFloat buttonWidth = [COMMON_SETTINGS getWidthForText:formSubmittedString withFont:cell.btnFormSubmitted.titleLabel.font andHeight:cell.btnFormSubmitted.frame.size.height minWidth:30];
//    cell.btnFormSubmittedWidth.constant = buttonWidth + 25;
//    [cell.btnFormSubmitted layoutIfNeeded];
//    
//    CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:cell.lblFieldText.text  withFont:cell.lblFieldText.font andWidth:cell.lblFieldText.frame.size.width minHeight:22];
//    
//    //cell.lblFieldTextHeightConstraint .constant = descriptionHeight;
//    //[cell.lblFieldText layoutIfNeeded];
//    
//    CAShapeLayer *labelView = [self GetLableForFormSubmitedBackgroundInFrame:cell.btnFormSubmitted.frame];
//    [cell.viewFormBack.layer addSublayer:labelView];
//    
//    labelView.fillColor = [UIColor colorWithRed:0.8784 green:0.6510 blue:0.0627 alpha:1.0].CGColor;
//    [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
//    CGRect frame = cell.contentView.frame;
//    frame.size.height = cell.lblFieldText.frame.origin.y + descriptionHeight + 30;
//    
//    cell.contentView.frame = frame;
    //[_baseScrollView addSubview:cell.contentView];
    
    [_baseScrollView addSubview:self.viewInfoDetailsHeader];
    
    _fieldsToShowArray = [_infromationDetailDictionary objectForKey:@""];
    
    //incrementedHeight = incrementedHeight + cell.contentView.frame.size.height ;
    incrementedHeight = incrementedHeight + self.viewInfoDetailsHeader.frame.size.height ;
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, 1)];
    line.backgroundColor = color_gray;
    [_baseScrollView addSubview:line];
    
    NSArray *imagesArray = [_infromationDetailDictionary objectForKey:@"lstImage"];
    NSArray *VideoArray = [_infromationDetailDictionary objectForKey:@"lstVideo"];
    NSArray *pdfArray = [_infromationDetailDictionary objectForKey:@"lstDocument"];
    NSArray *externalLinkArray = [_infromationDetailDictionary objectForKey:@"lstExternalLink"];
    NSArray *keywordsArray = [_infromationDetailDictionary objectForKey:@"lstKeyword"];
    NSString *descriptionHtmlString = [[_infromationDetailDictionary valueForKey:@"TopicDescription"] isKindOfClass:[NSNull class]]?@"":[_infromationDetailDictionary valueForKey:@"TopicDescription"];
    
    CGFloat headerHeight = 32;
    if(IS_IPAD)
        headerHeight = 40;
    
    //Information Description Implementation
    UILabel *descriptionSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, headerHeight)];
    [descriptionSectionTitle setBackgroundColor:[UIColor colorWithRed:0.0980 green:0.3686 blue:0.6314 alpha:1.0]];
    descriptionSectionTitle.font = app_font_bold_18;
    descriptionSectionTitle.textColor = color_white;
    descriptionSectionTitle.text = @"     Information Description";
    [_baseScrollView addSubview:descriptionSectionTitle];

    incrementedHeight = incrementedHeight + headerHeight + 5;
    CGFloat webViewHeight = 140;
    if(IS_IPAD)
        webViewHeight = 160;
    
    if(![descriptionHtmlString isKindOfClass:[NSNull class]])
    {
        if(test)
            test = nil;
        
        descriptionPage = [[UIWebView alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width - 30, webViewContentHeight+100)];
        descriptionPage.backgroundColor = [UIColor clearColor];
        descriptionPage.scrollView.scrollEnabled  = NO;
        //descriptionPage.scalesPageToFit = YES;
        descriptionPage.delegate = self;
        
        //NSString *htmlString = [NSString stringWithFormat:@"<font face='Lato-Regular' size='15'>%@", descriptionHtmlString];
        NSString *tblWidth = [NSString stringWithFormat:@"%f",(self.baseScrollView.frame.size.width - 40)];
        NSString *htmlString = [descriptionHtmlString stringByReplacingOccurrencesOfString:@"500px" withString:tblWidth];
        //[descriptionPage loadHTMLString:descriptionHtmlString baseURL:nil];
        [descriptionPage loadHTMLString:htmlString baseURL:nil];
        
        CGRect frame = descriptionPage.frame;
        frame.size.height = webViewContentHeight;
        descriptionPage.frame = frame;
        
        [_baseScrollView addSubview:descriptionPage];
        incrementedHeight = incrementedHeight + webViewContentHeight + 100;
    }
    
    if(imagesArray && [imagesArray count] > 0)
    {
        UILabel *imageSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, headerHeight)];
        [imageSectionTitle setBackgroundColor:[UIColor colorWithRed:0.0980 green:0.3686 blue:0.6314 alpha:1.0]];
        imageSectionTitle.font = app_font_bold_18;
        imageSectionTitle.textColor = color_white;
        imageSectionTitle.text = @"     All Images";
        [_baseScrollView addSubview:imageSectionTitle];
        
        incrementedHeight = incrementedHeight + headerHeight + 5;
        
        for (int i = 0 ; i < [imagesArray count]; i++) {
            //add images View
            UILabel *imageTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            imageTitle.numberOfLines = 3;
            imageTitle.font = app_font15;
            imageTitle.textColor = color_black;
            imageTitle.text = [[[imagesArray objectAtIndex:i] valueForKey:@"Text"] isKindOfClass:[NSNull class]]?@"...":[[imagesArray objectAtIndex:i] valueForKey:@"Text"];
            
            CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:imageTitle.text withFont: imageTitle.font andWidth: imageTitle.frame.size.width minHeight:30];
            CGRect frame = imageTitle.frame;
            frame.size.height = titleHeight;
            imageTitle.frame = frame;
            [_baseScrollView addSubview:imageTitle];
            incrementedHeight = incrementedHeight + titleHeight + 3;
            
            NSString *imageURL = [NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:i] valueForKey:@"DocumentPath"]];
            imageURL = [imageURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
            UIImage *image = [UIImage imageWithData:imageData];
            CGSize imageframe = image.size;
            UIImageView *imageView = [[UIImageView alloc] init];
            NSInteger maxWidth = _baseScrollView.frame.size.width - 20;
            if(imageframe.width > maxWidth)
            {
                NSInteger height = imageframe.height * maxWidth  / imageframe.width;
                imageView.frame = CGRectMake(0, incrementedHeight,maxWidth ,height );
            }
            else
            {
                imageView.frame = CGRectMake(0, incrementedHeight, imageframe.width,imageframe.height);
            }
            imageView.center = CGPointMake(_baseScrollView.frame.size.width/2, imageView.center.y);
            imageView.image = image;
            [_baseScrollView addSubview:imageView];
            
            incrementedHeight = incrementedHeight + imageView.frame.size.height + 10;
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementedHeight, _baseScrollView.frame.size.width-20, 1)];
            line.backgroundColor = color_lightGray;
            [_baseScrollView addSubview:line];
            
            incrementedHeight = incrementedHeight + line.frame.size.height +5;
        }
    }
    
    if(VideoArray && [VideoArray count] > 0)
    {
        UILabel *imageSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, headerHeight)];
        [imageSectionTitle setBackgroundColor:[UIColor colorWithRed:0.0980 green:0.3686 blue:0.6314 alpha:1.0]];
        imageSectionTitle.font = app_font_bold_18;
        imageSectionTitle.textColor = color_white;
        imageSectionTitle.text = @"     All Videos";
        [_baseScrollView addSubview:imageSectionTitle];
        
        incrementedHeight = incrementedHeight + headerHeight + 5;
        
        for (int i = 0 ; i < [VideoArray count]; i++) {
            //add images View
            UILabel *imageTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            imageTitle.numberOfLines = 3;
            imageTitle.font = app_font15;
            imageTitle.textColor = color_black;
            imageTitle.text = [[[VideoArray objectAtIndex:i] valueForKey:@"Text"] isKindOfClass:[NSNull class]]?@"...":[[VideoArray objectAtIndex:i] valueForKey:@"Text"];
            
            CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:imageTitle.text withFont: imageTitle.font andWidth: imageTitle.frame.size.width minHeight:30];
            CGRect frame = imageTitle.frame;
            frame.size.height = titleHeight;
            imageTitle.frame = frame;
            [_baseScrollView addSubview:imageTitle];
            incrementedHeight = incrementedHeight +  titleHeight;
            
            
            UILabel *UrlLinkLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            UrlLinkLabel.numberOfLines = 5;
            UrlLinkLabel.font = app_font10;
            if(IS_IPAD)
                UrlLinkLabel.font = app_font15;
            NSString *DocumentPath = [[VideoArray objectAtIndex:i] valueForKey:@"DocumentPath"];
            UrlLinkLabel.text = DocumentPath;
            CGFloat UrlLinkLabelHeight = [COMMON_SETTINGS getHeightForText:UrlLinkLabel.text withFont:UrlLinkLabel.font andWidth: UrlLinkLabel.frame.size.width minHeight:30];
            CGRect URLframe = UrlLinkLabel.frame;
            URLframe.size.height = UrlLinkLabelHeight;
            UrlLinkLabel.frame = URLframe;
            UrlLinkLabel.textColor = color_blueFont;
            
            UIButton *UrlLinkButton = [[UIButton alloc] initWithFrame:UrlLinkLabel.frame];
            [UrlLinkButton addTarget:self action:@selector(DidSelectLinkButton:) forControlEvents:UIControlEventTouchUpInside];
            [UrlLinkButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [UrlLinkButton setTitle:UrlLinkLabel.text forState:UIControlStateNormal];
            
            [_baseScrollView addSubview:UrlLinkLabel];
            [_baseScrollView addSubview:UrlLinkButton];
            
            incrementedHeight = incrementedHeight + UrlLinkLabelHeight + 10;
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementedHeight, _baseScrollView.frame.size.width-20, 1)];
            line.backgroundColor = color_lightGray;
            [_baseScrollView addSubview:line];
            
            incrementedHeight = incrementedHeight + line.frame.size.height +5;
        }
    }
    if(pdfArray && [pdfArray count] > 0)
    {
        UILabel *imageSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, headerHeight)];
        [imageSectionTitle setBackgroundColor:[UIColor colorWithRed:0.0980 green:0.3686 blue:0.6314 alpha:1.0]];
        
        imageSectionTitle.font = app_font_bold_18;
        imageSectionTitle.text = @"     All Documents";
        imageSectionTitle.textColor = color_white;
        [_baseScrollView addSubview:imageSectionTitle];
        
        incrementedHeight = incrementedHeight + headerHeight + 5;
        
        for (int i = 0 ; i < [pdfArray count]; i++) {
            //add images View
            UILabel *imageTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            imageTitle.numberOfLines = 3;
            imageTitle.font = app_font15;
            imageTitle.textColor = color_black;
            imageTitle.text = [[[pdfArray objectAtIndex:i] valueForKey:@"Text"] isKindOfClass:[NSNull class]]?@"...":[[pdfArray objectAtIndex:i] valueForKey:@"Text"];
            
            CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:imageTitle.text withFont: imageTitle.font andWidth: imageTitle.frame.size.width minHeight:30];
            CGRect frame = imageTitle.frame;
            frame.size.height = titleHeight;
            imageTitle.frame = frame;
            [_baseScrollView addSubview:imageTitle];
            incrementedHeight = incrementedHeight + titleHeight+3;
            
            
            UILabel *UrlLinkLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            UrlLinkLabel.numberOfLines = 3;
            UrlLinkLabel.font = app_font10;
            if(IS_IPAD)
                UrlLinkLabel.font = app_font15;
            NSString *DocumentPath = [[pdfArray objectAtIndex:i] valueForKey:@"DocumentPath"];
            UrlLinkLabel.text = DocumentPath;
            CGFloat UrlLinkLabelHeight = [COMMON_SETTINGS getHeightForText:UrlLinkLabel.text withFont:UrlLinkLabel.font andWidth: UrlLinkLabel.frame.size.width minHeight:30];
            CGRect URLframe = UrlLinkLabel.frame;
            URLframe.size.height = UrlLinkLabelHeight;
            UrlLinkLabel.frame = URLframe;
            UrlLinkLabel.textColor = color_blueFont;
            
            UIButton *UrlLinkButton = [[UIButton alloc] initWithFrame:UrlLinkLabel.frame];
            [UrlLinkButton addTarget:self action:@selector(DidSelectLinkButton:) forControlEvents:UIControlEventTouchUpInside];
            [UrlLinkButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [UrlLinkButton setTitle:UrlLinkLabel.text forState:UIControlStateNormal];
            
            [_baseScrollView addSubview:UrlLinkLabel];
            [_baseScrollView addSubview:UrlLinkButton];
            
            incrementedHeight = incrementedHeight + UrlLinkLabelHeight + 10;
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementedHeight, _baseScrollView.frame.size.width-20, 1)];
            line.backgroundColor = color_lightGray;
            [_baseScrollView addSubview:line];
            incrementedHeight = incrementedHeight + line.frame.size.height + 5;
        }
    }
    if(externalLinkArray && [externalLinkArray count] > 0)
    {
        UILabel *imageSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, headerHeight)];
        [imageSectionTitle setBackgroundColor:[UIColor colorWithRed:0.0980 green:0.3686 blue:0.6314 alpha:1.0]];
        imageSectionTitle.textColor = color_white;
        imageSectionTitle.font = app_font_bold_18;
        imageSectionTitle.text = @"     All External Links";
        [_baseScrollView addSubview:imageSectionTitle];
        
        incrementedHeight = incrementedHeight + headerHeight + 5;
        
        for (int i = 0 ; i < [externalLinkArray count]; i++) {
            //add images View
            UILabel *imageTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            imageTitle.numberOfLines = 3;
            imageTitle.font = app_font15;
            imageTitle.textColor = color_black;
            imageTitle.text = [[[externalLinkArray objectAtIndex:i] valueForKey:@"Text"] isKindOfClass:[NSNull class]]?@"...":[[externalLinkArray objectAtIndex:i] valueForKey:@"Text"];
            
            CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:imageTitle.text withFont: imageTitle.font andWidth: imageTitle.frame.size.width minHeight:30];
            CGRect frame = imageTitle.frame;
            frame.size.height = titleHeight;
            imageTitle.frame = frame;
            [_baseScrollView addSubview:imageTitle];
            incrementedHeight = incrementedHeight + titleHeight+3;
            
            
            UILabel *UrlLinkLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
            UrlLinkLabel.numberOfLines = 3;
            UrlLinkLabel.font = app_font10;
            if(IS_IPAD)
                UrlLinkLabel.font = app_font15;
            NSString *DocumentPath = [[externalLinkArray objectAtIndex:i] valueForKey:@"DocumentPath"];
            UrlLinkLabel.text = DocumentPath;
            CGFloat UrlLinkLabelHeight = [COMMON_SETTINGS getHeightForText:UrlLinkLabel.text withFont:UrlLinkLabel.font andWidth: UrlLinkLabel.frame.size.width minHeight:30];
            CGRect URLframe = UrlLinkLabel.frame;
            URLframe.size.height = UrlLinkLabelHeight;
            UrlLinkLabel.frame = URLframe;
            UrlLinkLabel.textColor = color_blueFont;
            
            UIButton *UrlLinkButton = [[UIButton alloc] initWithFrame:UrlLinkLabel.frame];
            [UrlLinkButton addTarget:self action:@selector(DidSelectLinkButton:) forControlEvents:UIControlEventTouchUpInside];
            [UrlLinkButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [UrlLinkButton setTitle:UrlLinkLabel.text forState:UIControlStateNormal];
            
            [_baseScrollView addSubview:UrlLinkLabel];
            [_baseScrollView addSubview:UrlLinkButton];
            
            incrementedHeight = incrementedHeight + UrlLinkLabelHeight + 10;
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementedHeight, _baseScrollView.frame.size.width-20, 1)];
            line.backgroundColor = color_lightGray;
            [_baseScrollView addSubview:line];
            incrementedHeight = incrementedHeight + line.frame.size.height + 5;
        }
    }
    
    if(keywordsArray && [keywordsArray count] > 0)
    {
        UILabel *keywordSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, incrementedHeight, _baseScrollView.frame.size.width, headerHeight)];
        [keywordSectionTitle setBackgroundColor:[UIColor colorWithRed:0.0980 green:0.3686 blue:0.6314 alpha:1.0]];
        keywordSectionTitle.textColor = color_white;
        keywordSectionTitle.font = app_font_bold_18;
        keywordSectionTitle.text = @"     All Keywords";
        [_baseScrollView addSubview:keywordSectionTitle];
        
        incrementedHeight = incrementedHeight + headerHeight + 5;
        
        UILabel *keywordTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, incrementedHeight, _baseScrollView.frame.size.width- 30, 0)];
        keywordTitle.numberOfLines = 5;
        keywordTitle.font = app_font15;
        keywordTitle.textColor = color_black;
        NSMutableArray *allKeywordsArray = [[NSMutableArray alloc] init];
        
        for (int i = 0 ; i < [keywordsArray count]; i++)
        {
            NSString *keyword = [[[keywordsArray objectAtIndex:i] valueForKey:@"keyword"] isKindOfClass:[NSNull class]]?@"...":[[keywordsArray objectAtIndex:i] valueForKey:@"keyword"];
            
            if(keyword.length != 0)
            {
                [allKeywordsArray addObject:keyword];
            }
        }
        
        if(allKeywordsArray.count>0)
        {
            keywordTitle.text = [NSString stringWithFormat:@"%@.",[allKeywordsArray componentsJoinedByString:@","]];
        }
        else
        {
            keywordTitle.text = @"";
        }
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:keywordTitle.text withFont: keywordTitle.font andWidth: keywordTitle.frame.size.width minHeight:30];
        CGRect frame = keywordTitle.frame;
        frame.size.height = titleHeight;
        keywordTitle.frame = frame;
        [_baseScrollView addSubview:keywordTitle];
        
        incrementedHeight = incrementedHeight + titleHeight+5;
        
        if(allKeywordsArray.count>0)
        {
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementedHeight, _baseScrollView.frame.size.width-20, 1)];
            line.backgroundColor = color_lightGray;
            [_baseScrollView addSubview:line];
            incrementedHeight = incrementedHeight + line.frame.size.height +5;
        }
    }
    
    _baseScrollView.contentSize = CGSizeMake(_baseScrollView.frame.size.width, incrementedHeight);
}

-(void)DidSelectLinkButton:(UIButton*)sender
{
    //NSLog(@"%@", [sender titleForState:UIControlStateNormal]);
    NSString *URLStr = [sender titleForState:UIControlStateNormal];
    URLStr = [URLStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *URL = [NSURL URLWithString:URLStr];
    [[UIApplication sharedApplication] openURL:URL];
    
}

#pragma mark - webview
- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    frame.size.width = _baseScrollView.frame.size.width - 30;
    aWebView.frame = frame;
    
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
    
    NSString *result = [aWebView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    
    webViewContentHeight = [result integerValue];
    
}

@end
