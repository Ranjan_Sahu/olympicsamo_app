//
//  SportsListingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "SportsListingViewController.h"

#define USE_MG_DELEGATE 1
@interface SportsListingViewController (){
    
    NSMutableArray *swipeCellButtons;
    NSDictionary *RecordDetailDictionery;
    NSMutableArray *SelectedRecordsArray;
    NSMutableArray *AllRecordsArray;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
}
@end

@implementation SportsListingViewController
@synthesize tableviewRecordList;

- (void)viewDidLoad {
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue91d"];
    self.automaticallyAdjustsScrollViewInsets = NO;

    // Do any additional setup after loading the view from its nib.
    [self callServiceToGetlslist];
    self.isNeedToRefresh=FALSE;
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
        [self callServiceToGetlslist];
    }
    
}
#pragma - mark WebService Call
-(void)callServiceToGetlslist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_SportDisciplineDetail withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}


-(void)callServiceDelete{
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"SportDisciplineId" : [RecordDetailDictionery valueForKey:@"strKey"]
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteSportDiscipline withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Delete;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if (_serviceCalled == Service_Called_Delete) {
        
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                    error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            [SelectedRecordsArray removeObjectAtIndex:currentIndexPathToDelete.row];
            [tableviewRecordList beginUpdates];
            [tableviewRecordList deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [tableviewRecordList endUpdates];
            currentIndexPathToDelete = nil;
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"strKey == %@",[responseDictionery valueForKey:@"SportDisciplineId"]];
            NSArray *filteredValues = [AllRecordsArray filteredArrayUsingPredicate:filterPredicate];
            if(filteredValues.count){
                NSInteger
                index = [AllRecordsArray indexOfObject:[filteredValues lastObject]];
                //NSLog(@"%ld",(long)index);
                NSMutableDictionary *singleRecord = [[NSMutableDictionary alloc] initWithDictionary:[filteredValues lastObject]];
                [singleRecord setValue:@0 forKey:@"IsSelected"];
                [AllRecordsArray replaceObjectAtIndex:index withObject:[NSDictionary dictionaryWithDictionary:singleRecord]];
            }
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if (_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                    error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstGridSport"] isKindOfClass:[NSNull class]])
            {
                NSArray * lstGridSport = [responseDictionery objectForKey:@"lstGridSport"];
                if(![lstGridSport isKindOfClass:[NSNull class]]){
                    SelectedRecordsArray = [[NSMutableArray alloc] initWithArray:lstGridSport];
                }
            }
            
            NSArray * lstSportChkBox = [responseDictionery objectForKey:@"lstSportChkBox"];
            if(![lstSportChkBox isKindOfClass:[NSNull class]]){
                AllRecordsArray = [[NSMutableArray alloc] initWithArray:lstSportChkBox];
            }
            //If Array Has the data then only Reload Data.
            if([SelectedRecordsArray count] > 0){
                [self.tableviewRecordList reloadData];
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/**************************** TABLEVIEW Functions ****************************************************/
/********************************************************************************************************/
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return SelectedRecordsArray.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [SelectedRecordsArray count]) {
        
        if(IS_IPAD) {
            return 70;
        }
        else{
            return 70;
        }
        
    }
    else{
        if(IS_IPAD) {
            return 84;
        }
        else{
            return 60;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row < [SelectedRecordsArray count])
    {
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewRecordList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        NSDictionary *dict = [SelectedRecordsArray objectAtIndex:indexPath.row];
        cell.lblAddressName.text = [dict valueForKey:@"strVal"];
        cell.lblDisplayAddress.hidden = TRUE;
        cell.lblLock.hidden = TRUE;
        cell.delegate = self;
        return cell;
    }
    else
    {
        static NSString *itemCell = @"FooterCustomCell";
        FooterCustomCell *cell = (FooterCustomCell *)[self.tableviewRecordList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FooterCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.titleLabel.text = @"Add Sports";
        }
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [SelectedRecordsArray count]){
        
        AddSportsViewController *AddSportsObj = [[AddSportsViewController alloc]initWithNibName:@"AddSportsViewController" bundle:nil];
        AddSportsObj.isLogedInUsersDetail = _isLogedInUsersDetail;
        AddSportsObj.playerDetails =  _playerDetails;
        AddSportsObj.serviceCalled = Service_Called_Add;
        AddSportsObj.RecordsArray = [[NSMutableArray alloc]initWithArray:AllRecordsArray];
        AddSportsObj.owner=self;
        [self.navigationController pushViewController:AddSportsObj animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


-(NSArray *) createRightButtons: (int) number height:(int)height width:(int)width
{
    NSMutableArray *result = [NSMutableArray array];
    NSString *titles[1] = {@"DELETE",};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *icons[1] = {delete};
    
    UIColor *colors[1] = {[UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1]};
    
    for (int i = 0; i < 1; ++i)
    {
        
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i] height:25 backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        //        frame.size.height = frame.size.height-30;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:data.rightButtonsCount height:cell.frame.size.height/4*3 width:cell.frame.size.height];
    }
}
#endif

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    
    NSIndexPath *indexPath = [self.tableviewRecordList indexPathForCell:cell];
    RecordDetailDictionery = [SelectedRecordsArray objectAtIndex:indexPath.row];
    
    
    if (index == 0) {
        // Show popup.
        
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete Your Sports Details?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         //Is User Wants to delete Details Then Continiue.
                                                                         //NSLog(@"Delete");
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDelete];
                                                                         
                                                                     }];
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // code to be written for version lower than ios 8.0...
        }
        //------------------------------------------------------------------------
        
    }
    
    return YES;
}

@end
