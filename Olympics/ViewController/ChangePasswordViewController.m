//
//  ViewController.m
//  Olympics
//
//  Created by webwerks on 05/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "HomeScreenViewController.h"
#import "EnterPinViewController.h"

@interface ChangePasswordViewController ()
{
    UITextField *currentTextField;
    BOOL isCurrentPinScreen,isNewPinScreen,isConfirmPinScreen;
    NSString *enteredPin,*confirmedPin;
}
@end

@implementation ChangePasswordViewController

#pragma - mark View Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isCurrentPinScreen = FALSE;
    [self SetFontForSubviews];
    
    UITapGestureRecognizer *tapOnBkView =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnBackGroundView:)];
    [self.view addGestureRecognizer:tapOnBkView];
    
    UITapGestureRecognizer *tapOnView =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnUserPinInView:)];
    [_viewUserPin addGestureRecognizer:tapOnView];
    
    [_txtUserPin addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
    [self clearTextField];
    
}

#pragma - mark Set Up subviews

-(void)SetFontForSubviews
{
    if(isConfirmPinScreen){
        _lblEnterPin.text = @"Re - Enter New Pin";
        _lblPinRecieved.text = @"Re - Enter 4 digit new Pin Number";
        [_btnVerify setTitle:@"CONFIRM" forState:UIControlStateNormal];
    }
    else if(isNewPinScreen){
        _lblEnterPin.text = @"Enter New Pin";
        _lblPinRecieved.text = @"Enter 4 digit new Pin Number";
        [_btnVerify setTitle:@"CONTINUE" forState:UIControlStateNormal];
    }
    else if (!isCurrentPinScreen){
        isCurrentPinScreen = TRUE;
        _lblEnterPin.text = @"Please Enter Current Pin";
        _lblPinRecieved.text = @"Enter your old Pin Number";
        [_btnVerify setTitle:@"VERIFY" forState:UIControlStateNormal];
    }
    
}

-(void)clearTextField
{
    _txtPin1.text = @"";
    _txtPin2.text = @"";
    _txtPin3.text = @"";
    _txtPin4.text = @"";
    _txtUserPin.text = @"";
    
}
#pragma - mark UITextField Deleget

- (void)textFieldChanged:(UITextField *)textField
{
    if (textField.text.length == 0) {
        self.txtPin1.text=@"";
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    switch (newString.length)
    {
        case 1:
            self.txtPin1.text = [newString substringWithRange:NSMakeRange(0, 1)];
            self.txtPin2.text=@"";
            self.txtPin3.text=@"";
            self.txtPin4.text=@"";
            break;
        case 2:
            self.txtPin2.text = [newString substringWithRange:NSMakeRange(1, 1)];
            self.txtPin3.text=@"";
            self.txtPin4.text=@"";
            break;
        case 3:
            self.txtPin3.text = [newString substringWithRange:NSMakeRange(2, 1)];
            self.txtPin4.text=@"";
            break;
        case 4:
            self.txtPin4.text = [newString substringWithRange:NSMakeRange(3, 1)];
            break;
        default:
            break;
    }
    return (newString.length<=4);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return TRUE;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return TRUE;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark Tap Gesture Delegate
- (void)tapOnUserPinInView:(UITapGestureRecognizer *)recognizer {
    [_txtUserPin becomeFirstResponder];
}
- (void)tapOnBackGroundView:(UITapGestureRecognizer *)recognizer {
    [_txtUserPin resignFirstResponder];
}

#pragma - mark IBActionMethods

- (IBAction)onClickVerify:(UIButton *)sender
{
    if([COMMON_SETTINGS validateOnlyNumeric:_txtUserPin.text])
    {
        if(_txtUserPin.text.length == 0)
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Please Enter pin" CancelButtonTitle:@"Ok" InView:self];
        }
        else if ((_txtUserPin.text.length > 0)&&(_txtUserPin.text.length < 4))
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Pin must be of 4 digits" CancelButtonTitle:@"Ok" InView:self];
            [self clearTextField];
        }
        else if(isCurrentPinScreen)
        {
            if([_txtUserPin.text isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UD_UserSecretPin]])
            {
                isCurrentPinScreen = FALSE;
                isNewPinScreen =TRUE;
                [self SetFontForSubviews];
                [self clearTextField];
            }
            else
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Current Pin is Invalid" CancelButtonTitle:@"Ok" InView:self];
                [self clearTextField];
            }
        }
        else if(isNewPinScreen)
        {
            if(![_txtUserPin.text isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UD_UserSecretPin]])
            {
                enteredPin = _txtUserPin.text;
                isNewPinScreen =FALSE;
                isConfirmPinScreen =TRUE;
                [self SetFontForSubviews];
                [self clearTextField];
            }
            else
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"You can't use old Pin" CancelButtonTitle:@"Ok" InView:self];
                [self clearTextField];
            }
        }
        else if(isConfirmPinScreen)
        {
            confirmedPin = _txtUserPin.text;
            if([enteredPin isEqual:confirmedPin])
            {
                [[NSUserDefaults standardUserDefaults] setValue:confirmedPin forKey:UD_UserSecretPin];
                isConfirmPinScreen = FALSE;
                if(self.presentingViewController)
                {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    EnterPinViewController *homeView = [[EnterPinViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"EnterPinViewController"] bundle:nil];
                    APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
                    APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
                    [APP_DELEGATE.window makeKeyAndVisible];

                }
                else
                {
                    for (UIViewController *viewcntrl in [self.navigationController viewControllers])
                    {
                        if ([viewcntrl class]==[EnterPinViewController class])
                        {
                            EnterPinViewController *homeView = [[EnterPinViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"EnterPinViewController"] bundle:nil];
                            APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
                            APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
                            [APP_DELEGATE.window makeKeyAndVisible];
                            break;
                        }
                        else
                        {
                            EnterPinViewController *homeView = [[EnterPinViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"EnterPinViewController"] bundle:nil];
                            APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
                            APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
                            [APP_DELEGATE.window makeKeyAndVisible];
                            break;
                        }
                    }
                }
            }
            else
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Confirm pin is not matched" CancelButtonTitle:@"Ok" InView:self];
            }
        }
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Only numeric values are allowed" CancelButtonTitle:@"Ok" InView:self];
    }
}

- (IBAction)DidSelectCancelButton:(id)sender {
    
    if(self.presentingViewController)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

@end
