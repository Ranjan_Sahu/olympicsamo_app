//
//  AddressListingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AddressListingViewController.h"

#define USE_MG_DELEGATE 1
@interface AddressListingViewController () {
    
    NSMutableArray *swipeCellButtons;
    NSMutableArray *arrAddressList;
    NSDictionary *addressDetails;
    UITableViewCellAccessoryType accessory;
    BOOL allowMultipleSwipe;
    NSArray *typesToshowArray;
    NSArray *statesToshowArray;
    NSArray *countriesToshowArray;
    NSIndexPath *currentIndexPathToDelete;
}

@end

@implementation AddressListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    swipeCellButtons = [TestData data];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue910"];
    _isFirstTime = true;
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view from its nib.
    self.isNeedToRefresh=FALSE;
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)DidSelectSaveButton:(id)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    if (self.isNeedToRefresh || _isFirstTime)
    {
        self.isNeedToRefresh=false;
        _isFirstTime = false;
        [self callServiceToGetAddresslist];
    }
    
}

#pragma - mark WebService Call
-(void)callServiceToGetAddresslist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ @"IsMyTeamMember": @false, @"OrgID": @0 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_AddressDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
    
}

-(void)callServiceDeleteAddress {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"AddressId" : [addressDetails valueForKey:@"AddressId"]
                                 
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteAddressDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Delete;
    
}

-(void)callServiceEditAddress {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"AddressId" : [addressDetails valueForKey:@"AddressId"]
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_EditAddressDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Edit;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    if (_serviceCalled == Service_Called_Delete) {
        
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            //After Getting Success_Response We need delete the Record from Tableview.
            [arrAddressList removeObjectAtIndex:currentIndexPathToDelete.row];
            [_tableviewAddreslist beginUpdates];
            [_tableviewAddreslist deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [_tableviewAddreslist endUpdates];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if (_serviceCalled == Service_Called_GetDetail) {
        
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstAddressGrid"] isKindOfClass:[NSNull class]])
            {
                arrAddressList = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstAddressGrid"]];
                
                if([arrAddressList count] > 0) {
                    [_tableviewAddreslist reloadData];
                }
            }
            
            typesToshowArray = [responseDictionery objectForKey:@"drpType"];
            statesToshowArray = [responseDictionery objectForKey:@"drpState"];
            countriesToshowArray = [responseDictionery objectForKey:@"drpCountry"];
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_Edit)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            
            AddAddressListViewController *address = [[AddAddressListViewController alloc]initWithNibName:@"AddAddressListViewController" bundle:nil];
            address.isLogedInUsersDetail = _isLogedInUsersDetail;
            address.playerDetails = _playerDetails;
            //Show, User Wants to add details or edit details.
            address.serviceCalled = Service_Called_Edit;
            currentIndexPathToDelete = nil;
            
            address.typesArray = [[NSArray alloc]initWithArray:typesToshowArray];
            address.statesArray = [[NSArray alloc] initWithArray:statesToshowArray];
            address.countriesArray = [[NSArray alloc] initWithArray:countriesToshowArray];
            address.addressDetails = responseDictionery;
            address.owner=self;
            [self.navigationController pushViewController:address animated:YES];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/**************************** TABLEVIEW Functions ****************************************************/
/********************************************************************************************************/
#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrAddressList.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrAddressList count]) {
        
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewAddreslist dequeueReusableCellWithIdentifier:itemCell];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        CGFloat cellHeight = 30.0f;
        NSDictionary *dictionary = [arrAddressList objectAtIndex:indexPath.row];
        NSString *descriptionString = [[dictionary valueForKey:@"Address"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"AddressType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cellHeight = titleHeight + descriptionHeight + cellHeight;
        return cellHeight;
        
    }
    else{
        if(IS_IPAD) {
            return 84;
        }
        else{
            return 60;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrAddressList count])
    {
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewAddreslist dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        NSDictionary *dictionary = [arrAddressList objectAtIndex:indexPath.row];
        
        NSString *descriptionString = [[dictionary valueForKey:@"Address"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *titleString = [[dictionary valueForKey:@"AddressType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cell.titleHeightConstraint.constant = titleHeight;
        cell.descriptionHeightConstraint.constant = descriptionHeight;
        
        bool IsPrimary =  [[dictionary valueForKey:@"IsPrimary"] boolValue];
        cell.lblLock.text = @"\ue903";
        
        cell.lblAddressName.text = titleString;
        cell.lblDisplayAddress.text = descriptionString;
        
        
        if (IsPrimary == true)
        {
            cell.lblLock.alpha = 0.7;
            cell.lblLock.hidden  = FALSE;
        } else {
            cell.lblLock.alpha = 0.5;
            cell.lblLock.hidden  = TRUE;
        }
        
        cell.delegate = self;
        return cell;
    }
    else
    {
        static NSString *itemCell = @"FooterCustomCell";
        FooterCustomCell *cell = (FooterCustomCell *)[self.tableviewAddreslist dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FooterCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.titleLabel.text = @"Add Address";
        }
        return cell;
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [arrAddressList count]){
        
        AddAddressListViewController *address = [[AddAddressListViewController alloc]initWithNibName:@"AddAddressListViewController" bundle:nil];
        //Show, User Wants to add details or edit details.
        address.serviceCalled = Service_Called_Add;
        
        address.typesArray = [[NSArray alloc]initWithArray:typesToshowArray];
        address.statesArray = [NSArray arrayWithArray:statesToshowArray];
        address.countriesArray = [NSArray arrayWithArray:countriesToshowArray];
        address.playerDetails = _playerDetails;
        address.owner=self;
        [self.navigationController pushViewController:address animated:YES];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

/************************************************************************/

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray *result = [NSMutableArray array];
    NSString* titles[2] = {@"DELETE", @"EDIT"};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *edit = [UIImage imageNamed:@"edit"];
    UIImage *icons[2] = {delete,edit};
    
    UIColor *colors[2] = {[UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1], [UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1]};
    
    for (int i = 0; i < 2; ++i)
    {
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        swipeSettings.onlySwipeButtons = YES;
        return [self createRightButtons:data.rightButtonsCount];
    }
    
}
#endif

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    NSIndexPath *indexPath = [self.tableviewAddreslist indexPathForCell:cell];
    addressDetails = [arrAddressList objectAtIndex:indexPath.row];
    
    // select edit or delete
    if (index == 0) {
        
        // Show popup.
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete Your Address Details?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         //Is User Wants to delete Address Details Then Continiue.
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDeleteAddress];
                                                                         
                                                                     }];
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // code to be written for version lower than ios 8.0...
        }
        //------------------------------------------------------------------------
        
    } else {
        [self callServiceEditAddress];
    }
    
    return YES;
}

@end
