//
//  HomeScreenViewController.m
//  Olympics
//
//  Created by webwerks on 07/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "HomeScreenViewController.h"
#import "DropDownCustomCell.h"
#import "MyGamesViewController.h"
#import "UIImageView+WebCache.h"
#import "NewTeamAgreementViewController.h"

@interface HomeScreenViewController ()
{
    int flag;
    CGPoint point1,point2,point3,point4;
    UIBezierPath *linePath;
    CAShapeLayer *line;
    BOOL isPersonalVisible, isMyTeamVisible, isInformationVisible, isAgreementVisible,isConsentGame;
    UIView *firstMenupartView;
    UIView *secondMenupartView;
    NSTimer *notificationCountTimer;
    NSString *thumProImageUrlString, *fullProImageUrlString;
}
@end

@implementation HomeScreenViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [_btnDropdown setTitle:@"\ue921" forState:UIControlStateNormal];
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo]];
    _lblUserName.text = [userInfo valueForKey:@"FullName"];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"IS_CONSENT_GAME"] boolValue] == TRUE) {
        NewTeamAgreementViewController *hsVC=[[NewTeamAgreementViewController alloc]initWithNibName:@"NewTeamAgreementViewController" bundle:nil];
        hsVC.playerDetails = userInfo;
        hsVC.isLogedInUsersDetail = TRUE;
        [self.navigationController pushViewController:hsVC animated:YES];
    }
    
    // Do any additional setup after loading the view.
    _dropDownView.hidden = TRUE;
    _dropDownTblView.delegate = self;
    _dropDownTblView.dataSource = self;
    flag = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)AddTilesOnScreen
{
    for (UIView *view in firstMenupartView.subviews) {
        [view removeFromSuperview];
    }
    [firstMenupartView removeFromSuperview];
    
    for (UIView *view in secondMenupartView.subviews) {
        [view removeFromSuperview];
    }
    [secondMenupartView removeFromSuperview];
    
    NSMutableArray *titleAndLogoList = [[NSMutableArray alloc] init];
    
    int menuPartViewx = 10;
    int menuPartViewy = 146;
    int menuPartViewWidth = SCREEN_WIDTH - 60;
    int menuPartViewHeight =  (menuPartViewWidth - menuPartViewx)/2;
    
    UIFont *allLableFont = app_font_bold_16;
    UIFont *allIcomoonFont = [UIFont fontWithName:@"icomoon" size:50.0];
    
    if(IS_IPHONE_5)
    {
        menuPartViewx = 10;
        menuPartViewy = 135;
        menuPartViewWidth = 300;
        menuPartViewHeight = (menuPartViewWidth - menuPartViewx)/2;
    }
    else if (IS_IPAD)
    {
        menuPartViewx = 20;
        menuPartViewy = 220;
        menuPartViewWidth = SCREEN_WIDTH - 200;
        menuPartViewHeight = (menuPartViewWidth - menuPartViewx)/2;
        
        allLableFont = app_font_bold_27;
        allIcomoonFont = [UIFont fontWithName:@"icomoon" size:90.0];
    }
    
    NSDictionary *record = @{@"Title":@"PERSONAL INFO",@"Logo":@"\ue90c",@"Tag":@"1"};
    [titleAndLogoList addObject:record];
    
    if(isAgreementVisible)
    {
        NSDictionary *record = @{@"Title":@"TEAM AGREEMENTS",@"Logo":@"\ue91a",@"Tag":@"2"};
        [titleAndLogoList addObject:record];
    }
    if(isInformationVisible)
    {
        NSDictionary *record = @{@"Title":@"INFORMATION",@"Logo":@"\ue90d",@"Tag":@"3"};
        [titleAndLogoList addObject:record];
    }
    if(isMyTeamVisible)
    {
        NSDictionary *record = @{@"Title":@"MY TEAM MEMBERS",@"Logo":@"\ue912",@"Tag":@"4"};
        [titleAndLogoList addObject:record];
    }
    
    NSInteger numberOfTiles = [titleAndLogoList count];
    if(numberOfTiles>0 )
    {
        firstMenupartView = [[UIView alloc] initWithFrame:CGRectMake(menuPartViewx ,menuPartViewy, menuPartViewWidth, menuPartViewHeight)];
        firstMenupartView.center = CGPointMake(SCREEN_WIDTH / 2, firstMenupartView.center.y);
        
        [self.view addSubview:firstMenupartView];
        
        if(numberOfTiles == 1)
        {
            UIView *firstRectCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, menuPartViewWidth, menuPartViewHeight/2-5)];
            [firstRectCell setBackgroundColor:[UIColor colorWithRed:0.1020 green:0.3882 blue:0.6706 alpha:1.0]];
            UILabel *firstRectLogoLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, firstRectCell.frame.size.height, firstRectCell.frame.size.height)];
            firstRectLogoLabel.textColor = color_white;
            firstRectLogoLabel.font = allIcomoonFont;
            firstRectLogoLabel.text = [[titleAndLogoList objectAtIndex:0] valueForKey:@"Logo"];
            [firstRectCell addSubview:firstRectLogoLabel];
            
            UILabel *firstRectTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstRectCell.frame.size.height + 12,0,225,firstRectCell.frame.size.height)];
            firstRectTitleLabel.textColor = color_white;
            firstRectTitleLabel.text = [[titleAndLogoList objectAtIndex:0] valueForKey:@"Title"];
            firstRectTitleLabel.numberOfLines = 2;
            firstRectTitleLabel.font = allLableFont;
            [firstRectCell addSubview:firstRectTitleLabel];
            
            UIButton *firstRectButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, firstRectCell.frame.size.width, firstRectCell.frame.size.height)];
            firstRectButton.tag = [[[titleAndLogoList objectAtIndex:0] valueForKey:@"Tag"] integerValue];
            [firstRectButton addTarget:self action:@selector(DidSelectMenuButton:) forControlEvents:UIControlEventTouchUpInside];
            [firstRectCell addSubview:firstRectButton];
            [firstMenupartView addSubview:firstRectCell];
            
        }
        else if(numberOfTiles > 1)
        {
            int reducedWidth = 50;
            if(IS_IPAD)
                reducedWidth = 100;
            
            UIView *firstCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, menuPartViewHeight, menuPartViewHeight)];
            [firstCell setBackgroundColor:[UIColor colorWithRed:0.1020 green:0.3882 blue:0.6706 alpha:1.0]];
            [firstMenupartView addSubview:firstCell];
            
            UILabel *firstLogoLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, firstCell.frame.size.height/2, firstCell.frame.size.height/2)];
            firstLogoLabel.textColor = color_white;
            firstLogoLabel.font = allIcomoonFont;
            firstLogoLabel.text = [[titleAndLogoList objectAtIndex:0] valueForKey:@"Logo"];
            [firstCell addSubview:firstLogoLabel];
            
            UILabel *firstTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12 ,firstLogoLabel.frame.origin.y +firstLogoLabel.frame.size.height,firstCell.frame.size.width - reducedWidth,firstLogoLabel.frame.size.height)];
            firstTitleLabel.textColor = color_white;
            firstTitleLabel.text = [[titleAndLogoList objectAtIndex:0] valueForKey:@"Title"];
            firstTitleLabel.numberOfLines = 2;
            firstTitleLabel.font = allLableFont;
            [firstCell addSubview:firstTitleLabel];
            
            UIButton *firstButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, firstCell.frame.size.width, firstCell.frame.size.height)];
            firstButton.tag = [[[titleAndLogoList objectAtIndex:0] valueForKey:@"Tag"] integerValue];
            [firstButton addTarget:self action:@selector(DidSelectMenuButton:) forControlEvents:UIControlEventTouchUpInside];
            [firstCell addSubview:firstButton];
            
            UIView *secondCell = [[UIView alloc] initWithFrame:CGRectMake(menuPartViewHeight + menuPartViewx , 0, menuPartViewHeight, menuPartViewHeight)];
            [secondCell setBackgroundColor:[UIColor colorWithRed:0.1176 green:0.4196 blue:0.7412 alpha:1.0]];
            [firstMenupartView addSubview:secondCell];
            
            UILabel *secondLogoLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, secondCell.frame.size.height/2, secondCell.frame.size.height/2)];
            secondLogoLabel.textColor = color_white;
            secondLogoLabel.font = allIcomoonFont;
            secondLogoLabel.text = [[titleAndLogoList objectAtIndex:1] valueForKey:@"Logo"];
            [secondCell addSubview:secondLogoLabel];
            
            UILabel *secondTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12 ,secondLogoLabel.frame.origin.y +secondLogoLabel.frame.size.height,secondCell.frame.size.width - 24,secondLogoLabel.frame.size.height)];
            secondTitleLabel.textColor = color_white;
            secondTitleLabel.text = [[titleAndLogoList objectAtIndex:1] valueForKey:@"Title"];
            secondTitleLabel.numberOfLines = 2;
            secondTitleLabel.font = allLableFont;
            [secondCell addSubview:secondTitleLabel];
            
            UIButton *secondButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, secondCell.frame.size.width, secondCell.frame.size.height)];
            secondButton.tag = [[[titleAndLogoList objectAtIndex:1] valueForKey:@"Tag"] integerValue];
            [secondButton addTarget:self action:@selector(DidSelectMenuButton:) forControlEvents:UIControlEventTouchUpInside];
            [secondCell addSubview:secondButton];
        }
        
        if(numberOfTiles == 3)
        {
            secondMenupartView = [[UIView alloc] initWithFrame:CGRectMake(menuPartViewx ,menuPartViewy+menuPartViewx+menuPartViewHeight, menuPartViewWidth, menuPartViewHeight)];
            secondMenupartView.center = CGPointMake(SCREEN_WIDTH / 2, secondMenupartView.center.y);
            
            [self.view addSubview:secondMenupartView];
            
            UIView *thirdRectCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, menuPartViewWidth, menuPartViewHeight/2-5)];
            [thirdRectCell setBackgroundColor:[UIColor colorWithRed:0.1294 green:0.4588 blue:0.7922 alpha:1.0]];
            [secondMenupartView addSubview:thirdRectCell];
            
            UILabel *thirdRectLogoLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, thirdRectCell.frame.size.height, thirdRectCell.frame.size.height)];
            thirdRectLogoLabel.textColor = color_white;
            thirdRectLogoLabel.font = allIcomoonFont;
            thirdRectLogoLabel.text = [[titleAndLogoList objectAtIndex:2] valueForKey:@"Logo"];
            [thirdRectCell addSubview:thirdRectLogoLabel];
            
            UILabel *thirdRectTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(thirdRectCell.frame.size.height + 12,0,225,thirdRectCell.frame.size.height)];
            thirdRectTitleLabel.textColor = color_white;
            thirdRectTitleLabel.text = [[titleAndLogoList objectAtIndex:2] valueForKey:@"Title"];
            thirdRectTitleLabel.numberOfLines = 2;
            thirdRectTitleLabel.font = allLableFont;
            [thirdRectCell addSubview:thirdRectTitleLabel];
            
            UIButton *thirdRectButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, thirdRectCell.frame.size.width, thirdRectCell.frame.size.height)];
            thirdRectButton.tag = [[[titleAndLogoList objectAtIndex:2] valueForKey:@"Tag"] integerValue];
            [thirdRectButton addTarget:self action:@selector(DidSelectMenuButton:) forControlEvents:UIControlEventTouchUpInside];
            [thirdRectCell addSubview:thirdRectButton];
        }
        else if (numberOfTiles == 4)
        {
            secondMenupartView = [[UIView alloc] initWithFrame:CGRectMake(menuPartViewx ,menuPartViewy+menuPartViewx+menuPartViewHeight, menuPartViewWidth, menuPartViewHeight)];
            secondMenupartView.center = CGPointMake(SCREEN_WIDTH / 2, secondMenupartView.center.y);
            
            [self.view addSubview:secondMenupartView];
            
            UIView *thirdCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, menuPartViewHeight, menuPartViewHeight)];
            [thirdCell setBackgroundColor:[UIColor colorWithRed:0.1294 green:0.4588 blue:0.7922 alpha:1.0]];
            [secondMenupartView addSubview:thirdCell];
            
            UILabel *thirdLogoLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, thirdCell.frame.size.height/2, thirdCell.frame.size.height/2)];
            thirdLogoLabel.textColor = color_white;
            thirdLogoLabel.font = allIcomoonFont;
            thirdLogoLabel.text = [[titleAndLogoList objectAtIndex:2] valueForKey:@"Logo"];
            [thirdCell addSubview:thirdLogoLabel];
            
            UILabel *thirdtTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12 ,thirdLogoLabel.frame.origin.y +thirdLogoLabel.frame.size.height,thirdCell.frame.size.width - 24,thirdLogoLabel.frame.size.height)];
            thirdtTitleLabel.textColor = color_white;
            thirdtTitleLabel.text = [[titleAndLogoList objectAtIndex:2] valueForKey:@"Title"];
            thirdtTitleLabel.numberOfLines = 2;
            thirdtTitleLabel.font = allLableFont;
            [thirdCell addSubview:thirdtTitleLabel];
            
            UIButton *thirdButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, thirdCell.frame.size.width, thirdCell.frame.size.height)];
            thirdButton.tag = [[[titleAndLogoList objectAtIndex:2] valueForKey:@"Tag"] integerValue];
            [thirdButton addTarget:self action:@selector(DidSelectMenuButton:) forControlEvents:UIControlEventTouchUpInside];
            [thirdCell addSubview:thirdButton];
            
            UIView *fourthCell = [[UIView alloc] initWithFrame:CGRectMake(menuPartViewHeight + menuPartViewx , 0, menuPartViewHeight, menuPartViewHeight)];
            [fourthCell setBackgroundColor:[UIColor colorWithRed:0.1490 green:0.5020 blue:0.8706 alpha:1.0]];
            [secondMenupartView addSubview:fourthCell];
            
            UILabel *fourthLogoLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, fourthCell.frame.size.height/2, fourthCell.frame.size.height/2)];
            fourthLogoLabel.textColor = color_white;
            fourthLogoLabel.font = allIcomoonFont;
            fourthLogoLabel.text = [[titleAndLogoList objectAtIndex:3] valueForKey:@"Logo"];
            [fourthCell addSubview:fourthLogoLabel];
            
            UILabel *fourthTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12 ,fourthLogoLabel.frame.origin.y +fourthLogoLabel.frame.size.height,fourthCell.frame.size.width - 24,fourthLogoLabel.frame.size.height)];
            fourthTitleLabel.textColor = color_white;
            fourthTitleLabel.text = [[titleAndLogoList objectAtIndex:3] valueForKey:@"Title"];
            fourthTitleLabel.numberOfLines = 2;
            fourthTitleLabel.font = allLableFont;
            [fourthCell addSubview:fourthTitleLabel];
            
            UIButton *fourthButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, fourthCell.frame.size.width, fourthCell.frame.size.height)];
            fourthButton.tag = [[[titleAndLogoList objectAtIndex:3] valueForKey:@"Tag"] integerValue];
            [fourthButton addTarget:self action:@selector(DidSelectMenuButton:) forControlEvents:UIControlEventTouchUpInside];
            [fourthCell addSubview:fourthButton];
        }
    }
    [self.view bringSubviewToFront:_dropDownBgImage];
    [self.view bringSubviewToFront:_imgUserPic];
    [self.view bringSubviewToFront:_btnDropdown];
}
#pragma - mark Set Up subviews
-(void)viewWillAppear:(BOOL)animated
{
    [self shouldShowDropDown:FALSE];
    [self.navigationController.navigationBar setHidden:TRUE];
    [self CallServiceTogetMainMenuVisibilityList];
    //[self SetImagesFromURL];
    
    notificationCountTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(callNotificationCount) userInfo:nil repeats:YES];

//    [timer fire];
    
//    if(_isNeedToRefresh){ 
//        [self CallServiceTogetNotificationCount];
//    }
}

-(void)callNotificationCount
{
    [self CallServiceTogetNotificationCount];
    [self SetImagesFromURL];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self SetUpSubviews];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [notificationCountTimer invalidate];
}

-(void)DidSelectMenuButton:(UIButton*)sender
{
    NSInteger tag = [sender tag];
    if(tag == 1)
    {
        [self onClickPersonalInfoBtn:sender];
    }
    else if(tag == 2)
    {
        [self onClickAgreementBtn:sender];
    }
    else if(tag == 3)
    {
        [self onClickInformationBtn:sender];
    }
    else if(tag == 4)
    {
        [self onClickMyTeamBtn:sender];
    }
}


#pragma - mark WebService Call
-(void)CallServiceTogetMainMenuVisibilityList
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ };
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetMainMenuVisibilityList withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceTogetNotificationCount
{
   // [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ };
    NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetNotificationCountDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[userinfo valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_NotificationCount;
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if( _serviceCalled == Service_Called_GetDetail)
    {
        isMyTeamVisible = [[responseDictionery valueForKey:@"IsMyTeamVisible"] boolValue];
        isAgreementVisible = [[responseDictionery valueForKey:@"IsAgreement"] boolValue];
        isInformationVisible = [[responseDictionery valueForKey:@"IsInformation"] boolValue];
        //thumProImageUrlString = [responseDictionery valueForKey:@"ThumbnailProfileImage"];
        fullProImageUrlString = [responseDictionery valueForKey:@"FullSizeProfileImage"];
        
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo]];
        isConsentGame = [[userInfo valueForKey:@"Is_Consent_Game"]boolValue];
        
        [self AddTilesOnScreen];
    }
    else if( _serviceCalled == Service_Called_NotificationCount)
    {
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            _messageCount = [responseDictionery objectForKey:@"NotificationCount"];
            if([_messageCount isKindOfClass:[NSNull class]])
                _messageCount = @"0";
            
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"ResourceID"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"ResourceID"] forKey:Resource_Id];
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"ThumbnailProfileImage"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"ThumbnailProfileImage"] forKey:UD_ThumbnailProfileImage];
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"FullSizeProfileImage"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"FullSizeProfileImage"] forKey:UD_FullSizeProfileImage];
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo]];
            
            [[NSUserDefaults standardUserDefaults] synchronize];

            thumProImageUrlString = [[NSUserDefaults standardUserDefaults] valueForKey:UD_ThumbnailProfileImage];
    
            
            BOOL isConsent = [[responseDictionery valueForKey:@"IsConsent"] boolValue];
            if(isConsent)
            {
                [userInfo setValue:[responseDictionery valueForKey:@"IsConsent"] forKey:@"IsConsent"];
                
                [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:UD_UserInfo];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                TeamAgreementsViewController *hsVC=[[TeamAgreementsViewController alloc]initWithNibName:@"TeamAgreementsViewController" bundle:nil];
                hsVC.playerDetails = userInfo;
                hsVC.isLogedInUsersDetail = TRUE;
                [self.navigationController pushViewController:hsVC animated:YES];
                //NSLog(@"Button Agreement press");
            }
            else{
                [self.dropDownTblView reloadData];
            }
        }
        else{
            NSLog(@"Error Occured ..!");
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
}


#pragma mark Touch Implementations
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self shouldShowDropDown:FALSE];
}

-(void)SetUpSubviews
{
    _btnDropdown.layer.borderWidth = 3.0;
    _btnDropdown.layer.borderColor = [UIColor whiteColor].CGColor;
    [_imgUserPic.layer setCornerRadius:_imgUserPic.bounds.size.width/2];
    [_imgUserPic.layer setMasksToBounds:YES];
}

-(void)SetImagesFromURL
{
//    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo];
//    NSString *thumbImage = [userInfo valueForKey:UD_ThumbnailProfileImage];
    NSString *GameURL = [[NSUserDefaults standardUserDefaults] objectForKey:Game_ImageURL];
    
    if(thumProImageUrlString && ![thumProImageUrlString isKindOfClass:[NSNull class]] && ![thumProImageUrlString isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_imgUserPic sd_setImageWithURL:[NSURL URLWithString:thumProImageUrlString] placeholderImage:[UIImage imageNamed:@"testImage"]];
        });

    }
    else
    {
        _imgUserPic.image = [UIImage imageNamed:@"testImage"];
    }
//    else if(thumbImage && ![thumbImage isKindOfClass:[NSNull class]] && ![thumbImage isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [_imgUserPic sd_setImageWithURL:[NSURL URLWithString:thumbImage] placeholderImage:[UIImage imageNamed:@"testImage"]];
//        });
//    }
    
    if(GameURL && ![GameURL isKindOfClass:[NSNull class]] && ![GameURL isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_gameImage sd_setImageWithURL:[NSURL URLWithString:GameURL] placeholderImage:[UIImage imageNamed:@"Sponser"]];
        });
    }
    else
    {
        _gameImage.image = [UIImage imageNamed:@"Logo_Black_big"];
    }
    
    /*
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo];
    NSString *thumbImage = [userInfo valueForKey:UD_ThumbnailProfileImage];
    
    dispatch_queue_t queue1 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue1, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbImage]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _imgUserPic.image = image;
            });
        }
    });
    
    //NSString *GameURL = [userInfo valueForKey:UD_GameImage];
    NSString *GameURL = [[NSUserDefaults standardUserDefaults] objectForKey:@Game_ImageURL];
    dispatch_queue_t queue2 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue2, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:GameURL]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _gameImage.image = image;
            });
        }
    });
     */
}

#pragma - mark Button Action

- (void)onClickMyTeamBtn:(UIButton *)sender {
    
    MyTeamListingViewController *hsVC=[[MyTeamListingViewController alloc]initWithNibName:@"MyTeamListingViewController" bundle:nil];
    [self.navigationController pushViewController:hsVC animated:YES];
    //NSLog(@"Button Myteam press");
}

- (void)onClickInformationBtn:(UIButton *)sender {
    
    InformationListViewController *hsVC=[[InformationListViewController alloc]initWithNibName:@"InformationListViewController" bundle:nil];
    [self.navigationController pushViewController:hsVC animated:YES];
    //NSLog(@"Button Information press");
}

- (void)onClickAgreementBtn:(UIButton *)sender {
    
    NewTeamAgreementViewController *hsVC=[[NewTeamAgreementViewController alloc]initWithNibName:@"NewTeamAgreementViewController" bundle:nil];
    NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
    hsVC.playerDetails = userinfo;
    hsVC.isLogedInUsersDetail = TRUE;
    [self.navigationController pushViewController:hsVC animated:YES];
    //NSLog(@"Button Agreement press");
}

- (void)onClickPersonalInfoBtn:(UIButton *)sender {
    
    PersonalDetailViewController *hsVC=[[PersonalDetailViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"PersonalDetailViewController"] bundle:nil];
    NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
    hsVC.playerDetails = userinfo;
    hsVC.isLogedInUsersDetail = TRUE;
    [self.navigationController pushViewController:hsVC animated:YES];
    //NSLog(@"Button Personal press");
}

- (IBAction)onClickDropDownBtn:(id)sender {
    
    //NSLog(@"Drop Down Clicked");
    
    if(flag == 0)
    {
        [self shouldShowDropDown:FALSE];
    }
    else if(flag == 1)
    {
        [self shouldShowDropDown:TRUE];
    }
}

// Drop Down Functinality Implementations
-(void)shouldShowDropDown:(BOOL)setShow
{
    _dropDownView.hidden = !setShow;
    self.dropDownBgImage.hidden = !setShow;
    flag = !setShow;
    if(setShow)
        [self drawArrow];
    else
        [self clearArrow];
}

#pragma mark - TableView Datasource and Delegates
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _dropDownTblView.bounds.size.height/4 ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DropDownCustomCell *cell = (DropDownCustomCell *)[self.dropDownTblView dequeueReusableCellWithIdentifier:@"DropDownCustomCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DropDownCustomCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
    }
    
    //[cell.lblTitleText setFont:app_font18];
    [cell.lblTitleText setTextColor:color_white];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if(!IS_IPAD){
        [cell.lblTitleText setFont:app_font18];
    }
    
    cell.viewMsgCount.hidden = TRUE;
    
    switch (indexPath.row) {
        case 0:
        {
            cell.lblTitleText.text = @"My Games";
        }
            break;
            
        case 1:
        {
            //cell.lblTitleText.text = [NSString stringWithFormat:@"Messages (%@)",self.messageCount];
            cell.lblTitleText.text = @"Messages";
            
            if ([self.messageCount integerValue] > 0) {
                cell.lblMsgCount.text = [NSString stringWithFormat:@"%@",self.messageCount];
                cell.viewMsgCount.hidden = FALSE;
                
                //Add Shadow to viewMsgCount
                cell.viewMsgCount.layer.shadowOffset = CGSizeMake(-1.0f, 1.0f);
                cell.viewMsgCount.layer.shadowRadius = 1.0f;
                cell.viewMsgCount.layer.shadowOpacity = 0.2;
                cell.viewMsgCount.layer.shadowColor = color_white.CGColor;
                
            }
            else {
                cell.viewMsgCount.hidden = TRUE;
            }
        }
            break;
        case 2:
        {
            cell.lblTitleText.text = @"Change Pin";
        }
            break;
        case 3:
        {
            cell.lblTitleText.text = @"Logout";
        }
            break;
            
        default:
            break;
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [ _dropDownTblView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
            MyGamesViewController *myGameVC=[[MyGamesViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"MyGamesViewController"] bundle:nil];
            [self presentViewController:myGameVC animated:YES completion:nil];
        }
            break;
            
        case 1:
        {
            NotificationListingViewController *notificationVC=[[NotificationListingViewController alloc]initWithNibName:@"NotificationListingViewController" bundle:nil];
            notificationVC.owner = self;
            [self.navigationController pushViewController:notificationVC animated:YES];
        }
            break;
        case 2:
        {
            ChangePasswordViewController *changePassword=[[ChangePasswordViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"ChangePasswordViewController"] bundle:nil];
            [self.navigationController pushViewController:changePassword animated:YES];
        }
            break;
        case 3:
        {
            [APP_DELEGATE RemoveUserDataAndSetRootViewController];
        }
            break;
            
        default:
            break;
    }
}

// Bezier Curve Implementation
-(void)drawArrow {
    NSInteger arrowSize;
    
    if(IS_IPAD){
        arrowSize = 17;
    }else{
        arrowSize = 13;
    }
    point1 = CGPointMake((_btnDropdown.frame.origin.x + _btnDropdown.frame.size.width*0.5f), (_btnDropdown.frame.origin.y + _btnDropdown.frame.size.height + 2));
    
    point2 = CGPointMake((_btnDropdown.frame.origin.x + 2) , (_btnDropdown.frame.origin.y +_btnDropdown.frame.size.height + arrowSize));
    
    point3 = CGPointMake((_btnDropdown.frame.origin.x + _btnDropdown.frame.size.width - 2) , (_btnDropdown.frame.origin.y + _btnDropdown.frame.size.height + arrowSize));
    
    line = [CAShapeLayer layer];
    linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:point1];
    [linePath addLineToPoint:point2];
    [linePath addLineToPoint:point3];
    [linePath addLineToPoint:point1];
    
    line.lineWidth = 1.5f;
    line.path=linePath.CGPath;
    line.fillColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:1.0].CGColor;
    line.strokeColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:1.0].CGColor;
    [[self.view layer] addSublayer:line];
    [self.view bringSubviewToFront:_dropDownView];
}

- (void)clearArrow {
    
    [line removeFromSuperlayer];
    [self.view setNeedsDisplay];
}
@end
