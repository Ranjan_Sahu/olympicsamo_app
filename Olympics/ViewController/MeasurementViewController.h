//
//  MeasurementViewController.h
//  Olympics
//
//  Created by webwerks on 12/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMTextField.h"
#import "OLMDatePicker.h"


@interface MeasurementViewController : UIViewController <UIGestureRecognizerDelegate,UIScrollViewDelegate,Service_delegate>


@property (weak, nonatomic) IBOutlet UIView *measurementContentView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property(nonatomic, assign) Service_Called serviceCalled;
@property (assign, nonatomic) CGPoint previousLocation;
@property (nonatomic, retain) NSMutableArray *fieldsToShowArray;
@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;
@property (strong, nonatomic) IBOutlet UIWebView *headerWebView;

@property (weak, nonatomic) IBOutlet UIView *SlideView;
@property (weak, nonatomic) IBOutlet UIImageView *SideviewImage;
@property (weak, nonatomic) IBOutlet UIButton *sideViewButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConstraintSlideViewTreallingSpace;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *rightScrollView;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthRightScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingSpaceSeperatorLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeaderView;

@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectSideViewButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;
- (IBAction)DidSelectBackButton:(id)sender;


@end
