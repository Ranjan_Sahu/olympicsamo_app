//
//  AdhocViewController
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Question_Object.h"
#import "OLMLableSectionTitle.h"
#import "OLMCheckBox.h"
#import "OLMSingleCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "OLMRadioButton.h"
#import "OLMDatePicker.h"
#import "Service.h"
#import "OLMRadioButtonTextKeyType.h"
#import "OLMSingleCheckBox.h"
#import "OLMTextView.h"
#import "AdhocCustomCell.h"

@interface AdhocViewController : UIViewController<Service_delegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *vanigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogi;
@property(nonatomic, assign) Service_Called serviceCalled;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;

@property (weak, nonatomic) IBOutlet UITableView *contentTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTableViewTop;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end
